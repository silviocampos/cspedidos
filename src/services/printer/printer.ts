
import { AuthService } from '../auth/auth.service'

import _ from '../../util/commands'
import moment from 'moment';

interface Chrome {

    sockets: {
        tcp: {
            create(callback): any,
            connect(socketId, ipPrinterAddress, portPrinter, callback): any,
            send(socketId, buffer, callback): any,
            disconnect(socketId, callback): any,
            close(socketId, callback): any
        }
    }

}

declare var Buffer: any
declare var chrome: Chrome;
declare var TextEncoder: any;

export class PrinterController {



    private buffer: any = ``;
    private _model: any = null
    private socketId = null;

    constructor(private auth: AuthService) { }

    public printer() {
        return {
            font: (type: 'a' | 'b' | 'c') => {
                this.buffer += _.TEXT_FORMAT[`TXT_FONT_${type.toUpperCase()}`]
                return this.printer()
            },
            align: (align: 'CENTER' | 'LEFT' | 'RIGHT') => {
                this.buffer += _.TEXT_FORMAT[`TXT_ALIGN_${align.toUpperCase()}`]
                return this.printer()
            },
            style: (type: "BOLD" | "ITALIC" | "UNDERL" | "UNDERL_2" | "BOLD_ITALIC" | "BOLD_ITALIC_UNDERL" | "BOLD_ITALIC_UNDERL_2" | "BOLD_UNDERL" | "BOLD_UNDERL_2" |
                "ITALIC_UNDERL" | "ITALIC_UNDERL_2" | "NORMAL") => {

                switch (type.toUpperCase()) {

                    case 'BOLD':
                        this.buffer += _.TEXT_FORMAT.TXT_BOLD_ON;
                        this.buffer += _.TEXT_FORMAT.TXT_ITALIC_OFF;
                        this.buffer += _.TEXT_FORMAT.TXT_UNDERL_OFF;
                        break;
                    case 'ITALIC':
                        this.buffer += _.TEXT_FORMAT.TXT_BOLD_OFF;
                        this.buffer += _.TEXT_FORMAT.TXT_ITALIC_ON;
                        this.buffer += _.TEXT_FORMAT.TXT_UNDERL_OFF;
                        break;
                    case 'UNDERL':
                        this.buffer += _.TEXT_FORMAT.TXT_BOLD_OFF;
                        this.buffer += _.TEXT_FORMAT.TXT_ITALIC_OFF;
                        this.buffer += _.TEXT_FORMAT.TXT_UNDERL_ON;
                        break;
                    case 'UNDERL_2':
                        this.buffer += _.TEXT_FORMAT.TXT_BOLD_OFF;
                        this.buffer += _.TEXT_FORMAT.TXT_ITALIC_OFF;
                        this.buffer += _.TEXT_FORMAT.TXT_UNDERL2_ON;
                        break;

                    case 'BOLD_ITALIC':
                        this.buffer += _.TEXT_FORMAT.TXT_BOLD_ON;
                        this.buffer += _.TEXT_FORMAT.TXT_ITALIC_ON;
                        this.buffer += _.TEXT_FORMAT.TXT_UNDERL_OFF;
                        break;
                    case 'BOLD_ITALIC_UNDERL':
                        this.buffer += _.TEXT_FORMAT.TXT_BOLD_ON;
                        this.buffer += _.TEXT_FORMAT.TXT_ITALIC_ON;
                        this.buffer += _.TEXT_FORMAT.TXT_UNDERL_ON;
                        break;
                    case 'BOLD_ITALIC_UNDERL_2':
                        this.buffer += _.TEXT_FORMAT.TXT_BOLD_ON;
                        this.buffer += _.TEXT_FORMAT.TXT_ITALIC_ON;
                        this.buffer += _.TEXT_FORMAT.TXT_UNDERL2_ON;
                        break;
                    case 'BOLD_UNDERL':
                        this.buffer += _.TEXT_FORMAT.TXT_BOLD_ON;
                        this.buffer += _.TEXT_FORMAT.TXT_ITALIC_OFF;
                        this.buffer += _.TEXT_FORMAT.TXT_UNDERL_ON;
                        break;
                    case 'BOLD_UNDERL_2':
                        this.buffer += _.TEXT_FORMAT.TXT_BOLD_ON
                        this.buffer += _.TEXT_FORMAT.TXT_ITALIC_OFF
                        this.buffer += _.TEXT_FORMAT.TXT_UNDERL2_ON
                        break;
                    case 'ITALIC_UNDERL':
                        this.buffer += _.TEXT_FORMAT.TXT_BOLD_OFF
                        this.buffer += _.TEXT_FORMAT.TXT_ITALIC_ON
                        this.buffer += _.TEXT_FORMAT.TXT_UNDERL_ON
                        break;
                    case 'ITALIC_UNDERL_2':
                        this.buffer += _.TEXT_FORMAT.TXT_BOLD_OFF
                        this.buffer += _.TEXT_FORMAT.TXT_ITALIC_ON
                        this.buffer += _.TEXT_FORMAT.TXT_UNDERL2_ON
                        break;

                    case 'NORMAL':
                    default:
                        this.buffer += _.TEXT_FORMAT.TXT_BOLD_OFF
                        this.buffer += _.TEXT_FORMAT.TXT_ITALIC_OFF
                        this.buffer += _.TEXT_FORMAT.TXT_UNDERL_OFF
                        break;
                }
                return this.printer()
            },
            size: (width, height) => {
                if (2 >= width && 2 >= height) {
                    this.buffer += _.TEXT_FORMAT.TXT_NORMAL;
                    if (2 == width && 2 == height) {
                        this.buffer += _.TEXT_FORMAT.TXT_4SQUARE;
                    } else if (1 == width && 2 == height) {
                        this.buffer += _.TEXT_FORMAT.TXT_2HEIGHT;
                    } else if (2 == width && 1 == height) {
                        this.buffer += _.TEXT_FORMAT.TXT_2WIDTH;
                    }
                } else {
                    this.buffer += _.TEXT_FORMAT.TXT_CUSTOM_SIZE(width, height);
                }
                return this.printer()
            },
            text: (text) => {
                this.buffer += text + _.EOL
                return this.printer()
            },
            cut: (part?) => {
                this.buffer += '\n\n\n\n' + _.PAPER[part ? 'PAPER_PART_CUT' : 'PAPER_FULL_CUT'];
                return this.printer()
            },
            esc: () => {
                this.buffer += '\n'
                return this.printer()
            },
            barcode: (code, type, options?) => {


                const getParityBit = function (str) {
                    var parity = 0, reversedCode = str.split('').reverse().join('');
                    for (var counter = 0; counter < reversedCode.length; counter += 1) {
                        parity += parseInt(reversedCode.charAt(counter), 10) * Math.pow(3, ((counter + 1) % 2));
                    }
                    return String((10 - (parity % 10)) % 10);
                }

                const codeLengthF = function (str) {
                    let buff = Buffer.from((str.length).toString(16), 'hex');
                    return buff.toString();
                }

                options = options || {};
                var width, height, position, font, includeParity;
                if (typeof width === 'string' || typeof width === 'number') { // That's because we are not using the options.object
                    width = width[2];
                    height = height[3];
                    position = position[4];
                    font = font[5];
                } else {
                    width = options.width;
                    height = options.height;
                    position = options.position;
                    font = options.font;
                    includeParity = options.includeParity !== false; // true by default
                }

                type = type || 'EAN13'; // default type is EAN13, may a good choice ?
                var convertCode = String(code), parityBit = '', codeLength = '';
                if (typeof type === 'undefined' || type === null) {
                    throw new TypeError('barcode type is required');
                }
                if (type === 'EAN13' && convertCode.length !== 12) {
                    throw new Error('EAN13 Barcode type requires code length 12');
                }
                if (type === 'EAN8' && convertCode.length !== 7) {
                    throw new Error('EAN8 Barcode type requires code length 7');
                }
                if (this._model === 'qsprinter') {
                    this.buffer += _.MODEL.QSPRINTER.BARCODE_MODE.ON;
                }
                if (this._model === 'qsprinter') {
                    // qsprinter has no BARCODE_WIDTH command (as of v7.5)
                } else if (width >= 2 || width <= 6) {
                    this.buffer += _.BARCODE_FORMAT.BARCODE_WIDTH[width];
                } else {
                    this.buffer += _.BARCODE_FORMAT.BARCODE_WIDTH_DEFAULT;
                }
                if (height >= 1 || height <= 255) {
                    this.buffer += _.BARCODE_FORMAT.BARCODE_HEIGHT(height);
                } else {
                    if (this._model === 'qsprinter') {
                        this.buffer += _.MODEL.QSPRINTER.BARCODE_HEIGHT_DEFAULT;
                    } else {
                        this.buffer += _.BARCODE_FORMAT.BARCODE_HEIGHT_DEFAULT;
                    }
                }
                if (this._model === 'qsprinter') {
                    // Qsprinter has no barcode font
                } else {
                    this.buffer += _.BARCODE_FORMAT[
                        'BARCODE_FONT_' + (font || 'A').toUpperCase()
                    ];
                }
                this.buffer += _.BARCODE_FORMAT[
                    'BARCODE_TXT_' + (position || 'BLW').toUpperCase()
                ];
                this.buffer += _.BARCODE_FORMAT[
                    'BARCODE_' + ((type || 'EAN13').replace('-', '_').toUpperCase())
                ];
                if (type === 'EAN13' || type === 'EAN8') {
                    parityBit = getParityBit(code);
                }
                if (type == 'CODE128' || type == 'CODE93') {
                    codeLength = codeLengthF(code);
                }
                this.buffer += codeLength + code + (includeParity ? parityBit : '') + '\x00'; // Allow to skip the parity byte
                if (this._model === 'qsprinter') {
                    this.buffer += _.MODEL.QSPRINTER.BARCODE_MODE.OFF;
                }
                return this.printer();

            },
            print: () => {


                return new Promise(async (resolve, reject) => {




                    try {
                        await this.socketCreate()
                        await this.connectPrinter()
                        await this.sendPrinterData()
                    } catch (error) {
                        reject(error)
                    }

                    resolve(true)

                })


            }
        }
    }

    private socketCreate() {

        return new Promise((resolve, reject) => {


            chrome.sockets.tcp.create(createSocket => {

                const { socketId } = createSocket

                if (socketId) {
                    this.socketId = socketId;
                    return resolve()
                } else {
                    this.socketId = null;
                    return reject({ error: 'Não foi possível criar Socket para conexão!' })
                }


            })


        })

    }

    private connectPrinter() {

        return new Promise((resolve, reject) => {

            chrome.sockets.tcp.connect(this.socketId, this.auth.terminal.printer.address, 9100, _result => {

                if (!_result) {
                    resolve()
                } else {
                    reject({ error: `Não foi possível conectar a impressoara com (${this.auth.terminal.printer.address}) verique o endereço se está correto.\n\nSe preferir tente desmarcar a opção impressao pelo app.` })
                }


            })

        })

    }

    private sendPrinterData() {

        return new Promise((resolve, reject) => {

            const buffer = new TextEncoder().encode(this.buffer).buffer

            chrome.sockets.tcp.send(this.socketId, buffer, _result => {

                this.buffer = ``

                chrome.sockets.tcp.disconnect(this.socketId, () => { this.socketId = null })
                chrome.sockets.tcp.close(this.socketId, () => this.socketId = null)

                this.socketId = null

                console.log('fim com scoketId', this.socketId)

                resolve()

            })

        })

    }

    public async printerOrder(order) {

        const processeItem = (item) => new Promise((resolve, reject) => {
            for (let i = 1; i <= item.qtd; i++) {

                const setContentPrinter = this.printer()
                    .font('a')
                    .align('CENTER')
                    .style('BOLD_UNDERL')
                    .size(1, 1)
                    .text("PATATI PATATA\n")
                    .align('RIGHT')
                    .text(`TERMINAL: ${order.terminal} SEQUENCIA: ${order.sequence} \n DATA: ${moment(order.created).format('DD/MM/YYYY HH:mm')}\n`)
                    .text(`PAGAMENTO: ${order.payments.map(({ payment, amount }) => `${String(payment)}`).join()}\n`)
                    .align('LEFT')
                    .text(`1 - ${item.complement}`)
                    .text(`${order.dmstatus == 'CA' ? '**** ITEM CANCELADO ****' : ''}`)
                    .align('CENTER')
                    .text(`${order.second ? ' -- 2. via --' : ''}`)
                    .cut()

                resolve(setContentPrinter)
            }
        })

        await Promise.all(order.items.map(processeItem))
        return await this.printer().print()
    }

    public printerTester() {


        return this.printer()
            .font('a')
            .align('CENTER')
            .style('BOLD_UNDERL')
            .size(1, 1)
            .text('PATATI PATATA')
            .text('         1         2         3         4        ')
            .text('         IMPRESSORA         ')
            .text(`         ${this.auth.terminal.printer.address}         `)
            .text(`         CONECTADA VIA APP COM SUCESSO         `)
            .cut()
            .print()

    }


}