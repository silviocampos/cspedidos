import { Injectable } from '@angular/core'
import { HttpClient, HttpHeaders } from '@angular/common/http'

import { AuthService } from '../auth/auth.service';

import { StorageService } from '../storage/storage.service';

import { Observable } from 'rxjs';

import { timeout } from 'rxjs/operators';
import { AlertController } from 'ionic-angular';
import moment from 'moment';

@Injectable()
export class ApiService {

  timerDefault: number = 30000


  private sync: boolean = false;

  constructor(
    private http: HttpClient,
    private auth: AuthService,
    private storage: StorageService,
    private alertCtrl: AlertController
  ) {


    // if you want your code to work everytime even though you leave the page
    // Observable.interval(10000).subscribe(async () => {

    //   let orders = await this.storage.get('orders') as any[]


    //   this.auth.orders = (orders) ? orders.length : 0

    //   if (orders && orders.length > 0) {

    //     if (this.sync === false) {

    //       this.sync = true;

    //       await this.solicitation.order({ ...orders[0] })

    //       orders[0].sync = true;

    //       this.sync = false;

    //       orders = orders.filter(order => order.sync !== true)

    //       this.storage.set('orders', orders)

    //     }

    //   }

    // });

  }

  get httpOptions() {
    const headers = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'x-access-token': this.auth.user.token || ''
      })
    }

    return headers;
  }

  private async baseURL() {
    return await this.storage.get('baseURL')
  }

  private async post(route: string, data?, timer?) {
    return this.http.post(`${await this.baseURL()}${route}`, { ...data }, { ...this.httpOptions })
      .pipe(timeout(timer || this.timerDefault))
      .toPromise()
  }

  private async get(route: string, referenceId?, timer?) {

    if (await this.baseURL())
      return this.http.get(`${await this.baseURL()}${route}/${referenceId || ''}`, { ...this.httpOptions })
        .pipe(timeout(timer || this.timerDefault))
        .toPromise()
  }

  public solicitation = {
    login: (data) => this.post('/', { ...data }),
    productList: (referenceId?) => this.get('/api/v1/product/category', referenceId || "0"),
    registerCash: (data) => this.post('/api/v1/register', { ...data }),
    paymentsList: () => this.get('/api/v1/payment'),
    orderList: () => this.get('/api/v1/order', this.auth.boxIsOpen),
    orderEntry: () => this.get('/api/v1/order/entry'),
    order: (data) => this.post('/api/v1/order', { ...data }),
    sat: (data) => this.post('/api/v1/order/sat', { ...data }),
    orderVendor: (data) => this.post('/api/v1/order/vendor', { ...data }),
    orderDetail: (id) => this.get('/api/v1/order/detail', id),
    closeCash: (data) => this.post(`/api/v1/register/${this.auth.boxIsOpen}/close`, { ...data }),
    register: () => this.get(`/api/v1/register`, this.auth.boxIsOpen),
    categories: () => this.get('/category'),
    isAdmin: (password) => this.post('/api/v1/user/checkAdmin', { password }),
    pickup: (amount) => this.post(`/api/v1/register/${this.auth.boxIsOpen}/pickup`, { terminal: { ...this.auth.terminal }, amount }),
    entry: (amount) => this.post(`/api/v1/register/${this.auth.boxIsOpen}/entry`, { terminal: { ...this.auth.terminal }, amount }),

    mananger: {
      person: () => this.get('/api/v1/person/user'),
      registers: (dates) => this.post('/api/v1/register/list', { ...dates }),
      printClose: (id) => this.post(`/api/v1/register/${id}/print`, { terminal: { ...this.auth.terminal } }),
      printProduct: (register: any) => this.post(`/api/v1/register/${(typeof register === 'object') ? 0 : register}/product`, { register: [].concat(register), terminal: { ...this.auth.terminal } }),
      operators: () => this.get('/op_stocks')
    },

    sale: {
      printer: (order) => this.post(`/api/v1/order/print/${order}`, { terminal: { ...this.auth.terminal }, firstPrint: true }),
      printerApp: (order) => this.post(`/api/v1/order/print/${order}`, { terminal: { ...this.auth.terminal }, app: true, firstPrint: true }),
      printer2Via: (sale) => this.post(`/api/v1/order/print/${sale._id}`, { terminal: { ...this.auth.terminal } }),
      delete: (data) => this.post(`/api/v1/order/cancel/item`, { ...data, terminal: { ...this.auth.terminal } }),
      change: (data) => this.post(`/api/v1/order/cancel/itemNewEntry`, { ...data, terminal: { ...this.auth.terminal } })
    },
    closeOrder: (order) => this.post(`/api/v1/order/close`, { order }),
    ping: () => this.post('/ping', {}),

    printerTest: () => this.post('/printerTest', { terminal: { ...this.auth.terminal } })
  }

  public handlerError({ error }) {

    let msg: string;

    if (!error)
      msg = 'Erro desconhecido!'
    else if (error.message)
      msg = error.originalMessage || error.message
    else if (typeof error === 'string')
      msg = error
    else
      msg = 'Erro de comunicação com servidor!'

    const _alert = this.alertCtrl.create({
      title: `Ops!`,
      message: `${msg}`,
      buttons: ['Fechar']
    })

    _alert.present()
  }
}
