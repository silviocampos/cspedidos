import { Injectable } from '@angular/core'
import { StorageService } from '../storage/storage.service';

@Injectable()
export class AuthService {

  public orders = 0;
  public user: any = {}
  public products: any[] = []
  public terminal = {
    branch: 1,
    branch_ref: 1,
    printer: {
      address: '',
      print: {
        openRegister: false,
        pickupMoney: true,
        entryChange: false,
        receiptAsItem: true,
        receiptComplete: false,
        closeRegister: true,
        app: true
      }
    },
    register: null,
    _id: null,
    baseURL: null,
    port: null,
    update: null,
    operacao_barraca: null,
    fechamento_cego: true
  }
  public boxIsOpen: boolean = false;
  public payments: any[] = [];

  public vendors: any = [];

  public search = { description: false, orderByDescription: false, openProductList: true }

  constructor(
    private storage: StorageService
  ) { }

  public async setUser(user) {
    await this.storage.set('user', user)
    this.user = { ...user }
  }

  public async setProducts(products) {
    await this.storage.set('products', [...products])
    this.products = [...products]
  }

  public async setTerminal(account_bank) {
    this.terminal = await this.storage.get('terminal')
    this.terminal.register = account_bank;
    await this.storage.set('terminal', { ...this.terminal })
  }

  public async setRegister(register) {
    await this.storage.set('register', register)
    this.boxIsOpen = register;
  }

  public async setPayments(payments) {
    await this.storage.set('payments', payments)
    this.payments = payments;
  }

  public async logout() {
    await this.storage.delete('user')
    this.user = {}
  }

  public registerVendor(vendor) {
    this.storage.set('vendors', vendor)
  }

}
