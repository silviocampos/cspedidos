import { Injectable } from '@angular/core'

import { Storage } from '@ionic/storage';

@Injectable()
export class StorageService {


  constructor(
    private storage: Storage
  ) { }

  public async get(key) {
    return await this.storage.get(key)
  }

  public async set(key, value) {
    return await this.storage.set(key, value)
  }


  public async add_order(order) {

    let data;

    let items = await this.storage.get('orders')

    order.sync = false;

    if (items) {
      data = [...items, order]
    } else {
      data = [order]
    }

    await this.storage.set('orders', data)
    return data
  }

  public async delete(key) {
    return await this.storage.remove(key)
  }

  public async clear() {
    return await this.storage.clear()
  }



}
