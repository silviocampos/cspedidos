import { CurrencyMaskConfig } from "ng2-currency-mask/src/currency-mask.config";

export const CustomCurrencyMaskConfig: CurrencyMaskConfig = {
  align: "left",
  allowNegative: true,
  decimal: ",",
  precision: 2,
  prefix: "R$ ",
  suffix: "",
  thousands: "."
};

export const ProductsListOffline = [{
  "name": "hero Product",
  "id_product": 1,
  "price": 75,
  "hero": "OMG This just came out today!",
  "image": "http://placehold.it/940x300/999/CCC"
}, {
  "name": "Product 1",
  "id_product": 2,
  "price": 2,
  "info": "This is the latest and greatest product from Derp corp.",
  "image": "http://placehold.it/300x300/999/CCC"
}, {
  "name": "Product 2",
  "id_product": 3,
  "price": 3.50,
  "offer": "BOGOF",
  "image": "http://placehold.it/300x300/999/CCC"
}, {
  "name": "Product 3",
  "id_product": 4,
  "price": 5.99,
  "image": "http://placehold.it/300x300/999/CCC"
}, {
  "name": "Product 4",
  "id_product": 5,
  "price": 12.58,
  "offer": "No srsly GTFO",
  "image": "http://placehold.it/300x300/999/CCC"
}, {
  "name": "Product 5",
  "id_product": 6,
  "price": 4.75,
  "image": "http://placehold.it/300x300/999/CCC"
}, {
  "name": "Product 6",
  "id_product": 7,
  "price": 18.96,
  "info": "This is the latest and greatest product from Derp corp.",
  "offer": "info with offer",
  "image": "http://placehold.it/300x300/999/CCC"
}]
