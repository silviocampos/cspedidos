import * as _ from 'lodash';

const Objects = {
  clone: (objectOrArray) => _.cloneDeep(objectOrArray)
}


export default Objects
