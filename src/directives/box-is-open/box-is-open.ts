import { Directive } from '@angular/core';
import { AuthService } from '../../services/auth/auth.service';
import { NavController } from 'ionic-angular';
import { OpenCashRegisterPage } from '../../pages/open-cash-register/open-cash-register';


@Directive({
  selector: '[box-is-open]' // Attribute selector
})
export class BoxIsOpenDirective {

  constructor(
    private auth: AuthService,
    private navCtrl: NavController
  ) {

    if (this.auth.user.isAdmin === false && !this.auth.boxIsOpen)
      this.navCtrl.setRoot(OpenCashRegisterPage)

  }

}
