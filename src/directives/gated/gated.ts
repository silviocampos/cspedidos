import { Directive } from '@angular/core';
import { AuthService } from '../../services/auth/auth.service';
import { NavController } from 'ionic-angular';
import { LoginPage } from '../../pages/login/login';


@Directive({
  selector: '[gated]'
})
export class GatedDirective {

  constructor(
    private auth: AuthService,
    private navCtrl: NavController
  ) {
    if (!this.auth.user || !this.auth.user.token)
      this.navCtrl.setRoot(LoginPage)
  }

}
