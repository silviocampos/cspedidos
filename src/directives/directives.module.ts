import { NgModule } from '@angular/core';
import { GatedDirective } from './gated/gated';
import { BoxIsOpenDirective } from './box-is-open/box-is-open';
@NgModule({
	declarations: [GatedDirective,
    BoxIsOpenDirective],
	imports: [],
	exports: [GatedDirective,
    BoxIsOpenDirective]
})
export class DirectivesModule {}
