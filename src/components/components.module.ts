import { NgModule } from '@angular/core';
import { SaleInfoComponent } from './sale-info/sale-info';
import { CommonModule } from '@angular/common';
import { IonicModule } from 'ionic-angular';
@NgModule({
  declarations: [SaleInfoComponent,
  ],
  imports: [CommonModule, IonicModule],
  exports: [SaleInfoComponent,
  ]
})
export class ComponentsModule { }
