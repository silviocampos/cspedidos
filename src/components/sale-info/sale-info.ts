import { Component, ElementRef, ViewChild, Input, Renderer } from '@angular/core';
import { ApiService } from '../../services/api/api.service';
import { Events, AlertController, NavController, LoadingController, Loading, ModalController } from 'ionic-angular';
import { AuthService } from '../../services/auth/auth.service';
import { SalesSummaryPage } from '../../pages/sales-summary/sales-summary';
import { HomePage } from '../../pages/home/home';
import { FiscalPage } from '../../pages/fiscal/fiscal';

@Component({
  selector: 'sale-info',
  templateUrl: 'sale-info.html'
})
export class SaleInfoComponent {

  @ViewChild('expandWrapper', { read: ElementRef }) expandWrapper;
  @Input('expanded') expanded;
  @Input('expandHeight') expandHeight;
  @Input('saleInfo') saleInfo: any = { items: [], payments: [] }
  @Input('sale') sale: any = {}
  @Input('enabledCancel') enabledCancel: any;

  itemsSelected: any[] = [];
  loading: Loading;

  constructor(
    public renderer: Renderer,
    public api: ApiService,
    public events: Events,
    public alertCtrl: AlertController,
    public auth: AuthService,
    public navCtrl: NavController,
    public loadingCtrl: LoadingController,
    public modalCtrl: ModalController,
  ) {

  }

  ngAfterViewInit() {
    this.renderer.setElementStyle(this.expandWrapper.nativeElement, 'height', this.expandHeight + '%');
  }


  selectItem($item) {

    const isItemSelected = this.itemsSelected.findIndex(item => item._id === $item._id)

    if (isItemSelected === -1)
      this.itemsSelected.unshift($item)
    else
      this.itemsSelected.splice(isItemSelected, 1)

  }


  setLoading() {
    this.loading = this.loadingCtrl.create({ content: 'carregando...', dismissOnPageChange: true })
    this.loading.present()
  }

  async doCancel(sale) {



    let order: any = {
      register: this.auth.boxIsOpen,
      customer: 999,
      items: [...this.itemsSelected.map(item => {
        const { id_product, name, unit, price, pccod } = this.auth.products.find(product => product.name === item.complement)
        return {
          product: id_product,
          complement: name,
          qtd: item.qtd,
          unit,
          price,
          amount: price * item.qtd,
          pccod,
          _id: item._id,
          owner: item.owner
        }

      })],
      payments: [...this.auth.payments.filter(payment => payment.method.includes("DIN")).map(({ _id, pccod, docref, acronym, method }) => {
        return { payment: _id, pccod, docref, acronym, method, amount: this.itemsSelected.reduce((acc, obj) => acc + obj.price * obj.qtd, 0), change_money: 0 }
      })],
      summary: {
        amount: this.itemsSelected.reduce((acc, obj) => acc + obj.price * obj.qtd, 0),
        discount: 0,
        increase: 0,
        change: 0
      },
      user: this.auth.user._id
    }

    const handler: any = async ({ password }) => {

      this.setLoading()

      const { isAdmin } = await this.api.solicitation.isAdmin(password) as any
      if (!isAdmin) {
        this.loading.dismiss()
        return this.api.handlerError({ error: `Senha admin incorreta! tente novamente.` })
      }

      if (isAdmin === true) {

        try {
          await this.api.solicitation.sale.change({ order, order_ref: sale._id })
          this.navCtrl.pop();
          this.navCtrl.push(SalesSummaryPage)
        } catch (error) {
          this.loading.dismiss()
          return this.api.handlerError(error)
        }


      }

    }

    const _alert = this.alertCtrl.create({
      title: `Permissão`,
      message: `Digite a senha do administrador/gerente`,
      inputs: [
        {
          type: 'password',
          id: 'password',
          name: 'password'
        }
      ],
      buttons: [

        {
          text: `Cancelar`,
          role: 'cancel'
        },
        {
          text: 'confirmar',
          handler
        }
      ]
    })

    _alert.present()

  }

  async printer(sale) {

    const _alert_success = this.alertCtrl.create({
      title: 'Ok',
      message: `2º Via impressa com sucesso.`,
      buttons: ['Fechar']
    })

    const handler: any = async ({ password }) => {

      this.setLoading()

      const { isAdmin } = await this.api.solicitation.isAdmin(password) as any
      if (!isAdmin) {
        this.loading.dismiss()
        return this.api.handlerError({ error: `Senha admin incorreta! tente novamente.` })
      }

      if (isAdmin === true) {

        try {
          await this.api.solicitation.sale.printer2Via(sale)
          await this.loading.dismiss()
          await _alert_success.present()
        } catch (error) {
          this.loading.dismiss()
          return this.api.handlerError(error)
        }


      }

    }

    const _alert = this.alertCtrl.create({
      title: `Permissão`,
      message: `Digite a senha do administrador/gerente`,
      inputs: [
        {
          type: 'password',
          id: 'password',
          name: 'password'
        }
      ],
      buttons: [

        {
          text: `Cancelar`,
          role: 'cancel'
        },
        {
          text: 'confirmar',
          handler
        }
      ]
    })

    _alert.present()




  }

  async fiscal(sale) {
    this.navCtrl.push(FiscalPage, { sale })
    this.expanded = false
  }

  async doChange(sale) {


    const handler: any = async ({ password }) => {

      this.setLoading()

      const { isAdmin } = await this.api.solicitation.isAdmin(password) as any

      if (!isAdmin) {
        this.loading.dismiss()
        return this.api.handlerError({ error: `Senha admin incorreta! tente novamente.` })
      }

      if (isAdmin === true) {

        try {

          await this.loading.dismiss()


          const payments = this.auth.payments.filter(payment => payment.method.includes(sale.info.payments[0].payment)).map(payment => ({
            ...payment, amount: this.itemsSelected.map(item => {
              const { price, } = this.auth.products.find(product => product.name === item.complement)
              return price

            }).reduce((acc, value) => acc + value, 0)
          }))

          this.navCtrl.popAll().then(() => {
            const sale_items = Object.assign({}, { ...sale, info: { items: this.itemsSelected } })
            setTimeout(() => this.events.publish("change", { sale: payments, productsToCancel: sale_items }), 100)
          })




        } catch (error) {
          this.loading.dismiss()
          return this.api.handlerError(error)
        }


      }

    }


    const _alert = this.alertCtrl.create({
      title: `Permissão`,
      message: `Digite a senha do administrador/gerente`,
      inputs: [
        {
          type: 'password',
          id: 'password',
          name: 'password'
        }
      ],
      buttons: [

        {
          text: `Cancelar`,
          role: 'cancel'
        },
        {
          text: 'confirmar',
          handler
        }
      ]
    })

    _alert.present()



  }


}
