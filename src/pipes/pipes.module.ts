import { NgModule } from '@angular/core';
import { SearchPipe } from './search/search';
import { ColorsPipe } from './colors/colors';
import { OrderByPipe } from './search/order';
import { PaymentOrderPipe } from './payment.order.pipe';
@NgModule({
	declarations: [SearchPipe,
		ColorsPipe, OrderByPipe, PaymentOrderPipe],
	imports: [],
	exports: [SearchPipe,
		ColorsPipe, OrderByPipe, PaymentOrderPipe]
})
export class PipesModule { }
