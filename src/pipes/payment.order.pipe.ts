import { Pipe, PipeTransform } from '@angular/core';

/**
 * Generated class for the SearchPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
    name: 'paymentOrder',
})
export class PaymentOrderPipe implements PipeTransform {
    /**
     * Takes a value and makes it lowercase.
     */
    transform(arr: any[]) {


        if (!arr)
            return []


        const orderBy = (arr: any[]) => arr.sort((a, b) => {
            if (a.method.trim() < b.method.trim()) return 1
            if (a.method.trim() > b.method.trim()) return -1;
            return 0
        })


        return orderBy(arr)





    }
}
