import { Pipe, PipeTransform } from '@angular/core';

/**
 * Generated class for the SearchPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'search',
})
export class SearchPipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  transform(arr: any[], value = "", productList = false) {


    if (!value)
      return arr

    if (!arr)
      return []


    if (productList) {

      const filterArr = arr.filter(product => String(product.description).toLowerCase().includes(value.toLowerCase()) || String(product.name).toLowerCase().includes(value.toLowerCase()))
      return filterArr;

    }


    return arr.filter(item => {
      if (String(item.sequence).includes(String(value)))
        return true;

    })

  }
}
