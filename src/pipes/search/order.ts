import { Pipe, PipeTransform } from '@angular/core';
import { orderBy as orderByLodash } from 'lodash'
/**
 * Generated class for the SearchPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
    name: 'orderBy',
})
export class OrderByPipe implements PipeTransform {
    /**
     * Takes a value and makes it lowercase.
     */
    transform(arr: any[], orderBy = false) {

        if (!arr)
            return []


        if (orderBy) {

            let items = [];

            let subCategories = Array.from(new Set([...arr.map(item => item.subcategory)]))

            for (let category of subCategories) {
                items = [...items, ...orderByLodash(arr.filter(item => item.subcategory == category), "description")]
            }
            return items;

        } else {
            return arr
        }



    }
}
