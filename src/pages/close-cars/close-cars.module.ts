import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CloseCarsPage } from './close-cars';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    CloseCarsPage,
  ],
  imports: [
    PipesModule,
    IonicPageModule.forChild(CloseCarsPage),
  ],
})
export class CloseCarsPageModule { }
