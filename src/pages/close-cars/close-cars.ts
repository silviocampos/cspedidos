import { Component } from '@angular/core';
import { IonicPage, AlertController, LoadingController } from 'ionic-angular';
import { AuthService } from '../../services/auth/auth.service';
import { ApiService } from '../../services/api/api.service';

import * as moment from 'moment'

@IonicPage()
@Component({
  selector: 'page-close-cars',
  templateUrl: 'close-cars.html',
})
export class CloseCarsPage {

  products = [];
  category;
  loading: any;

  date = moment().format('YYYY-MM-DD')

  constructor(
    private auth: AuthService,
    private api: ApiService,
    private alertCtrl: AlertController,
    private loadingCtrl: LoadingController
  ) {
  }

  async ngOnInit() {
    const data = await this.api.solicitation.categories() as any

    this.category = data.filter(category => category.description.includes('ESTACIONAMENTO'))

    if (this.category.length > 0) {
      this.products = await this.api.solicitation.productList(this.category[0]._id) as any;
    }

  }

  setLoading() {
    this.loading = this.loadingCtrl.create({ content: 'carregando...', dismissOnPageChange: true })
    this.loading.present()
  }

  send() {

    this.setLoading()

    if (this.products.filter(prod => Number(prod.qtd) > 0).length === 0)
      return;

    const paymentDinheiro = this.auth.payments.find(payment => payment.acronym === 'DIN')

    const { _id, pccod, docref, acronym, method } = paymentDinheiro;


    const payments = { payment: _id, pccod, docref, acronym, method, amount: this.products.filter(prod => Number(prod.qtd) > 0).map(p => p.price * Number(p.qtd)).reduce((a, b) => a + b, 0), change_money: 0 }



    const sendOrder = async () => {

      let order: any = {
        register: 0,
        customer: 999,
        items: [...this.products.filter(prod => Number(prod.qtd) > 0).map(item => { return { complement: item.name, pccod: item.pccod || null, product: item.id_product, price: item.price, qtd: Number(item.qtd), amount: Number(item.price) * Number(item.qtd) } })],
        payments: [payments],
        summary: {
          amount: this.products.filter(prod => Number(prod.qtd) > 0).map(p => p.price * Number(p.qtd)).reduce((a, b) => a + b, 0),
          discount: 0,
          increase: 0,
          change: 0
        },
        user: this.auth.user._id,
        vendor: this.auth.user._id,
        vendor_name: this.auth.user.login,
        ticket: 0,
        date: this.date
      }


      try {

        await this.api.solicitation.orderVendor(
          {
            order,
            credit: 0,
            debit: 0,
            terminal: {
              ...this.auth.terminal
            }
          }
        )

      } catch (error) {
        this.loading.dismiss()
        return this.api.handlerError(error)
      }
      this.loading.dismiss()

      alert('Estacionamento enviado!')
      this.ngOnInit()

    }


    const _alert = this.alertCtrl.create({
      title: 'Confimar operação',
      message: `Deseja finalizar?`,
      buttons: [
        {
          text: 'Sim',
          handler: () => sendOrder() as any
        },
        {
          text: 'Ainda não',
          role: 'cancel'
        }
      ]
    })


    return _alert.present()

  }


}
