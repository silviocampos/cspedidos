import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';

import { ApiService } from '../../services/api/api.service';
import { AuthService } from '../../services/auth/auth.service';
import { StorageService } from '../../services/storage/storage.service';

import moment from 'moment';
import { LoginPage } from '../login/login';
import { OpenCashRegisterPage } from '../open-cash-register/open-cash-register';

@Component({
  selector: 'page-close-cash-register',
  templateUrl: 'close-cash-register.html',
})
export class CloseCashRegisterPage {

  formCloseCash: FormGroup;
  date = moment()
  registerCash: any = {};
  calculated: boolean = false

  confirmed: boolean = false;

  loading: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    public api: ApiService,
    public auth: AuthService,
    public storage: StorageService,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController
  ) { }

  async ngOnInit() {

    this.formCloseCash = this.formBuilder.group({
      value: [null, Validators.required],
    })


    this.setLoading()

    try {
      this.registerCash = await this.api.solicitation.register()



      if (this.registerCash.close != null) {

        const handler: any = async () => {
          await this.storage.delete('blockRegister')

          this.navCtrl.setRoot(OpenCashRegisterPage)
        }

        const _alert = this.alertCtrl.create({
          title: 'Ops!',
          message: `Parece que seu caixa já foi fechado, peça ao seu gerente/administrador para emitir a 2 via do fechamento indo em MENU > FECHAMENTOS DO DIA > SELECIONANDO O FECHAMENTO N° ${this.registerCash.register}`,
          buttons: [{
            text: 'Sair',
            handler
          }]
        })

        this.loading.dismiss()

        _alert.present()

        return;

      }

      if (this.registerCash.details)
        this.registerCash.details = this.registerCash.details.map(detail => { return { ...detail, filled: null } })

    } catch (error) {
      this.loading.dismiss()
      this.api.handlerError(error)
    }

    this.loading.dismiss()

  }

  setLoading() {
    this.loading = this.loadingCtrl.create({ content: 'carregando...', dismissOnPageChange: true })
    this.loading.present()
  }

  async doCloseCash() {

    if (this.confirmed === false && this.registerCash.qt_orders > 0) {

      const _alert = this.alertCtrl.create({
        title: 'Confirme',
        message: `Confirme os valores informados antes de fechar o caixa.`,
        buttons: [
          {
            text: 'Ok'
          }
        ]
      })

      _alert.present()


      this.confirmed = true;
      return false;

    }


    this.setLoading()

    try {
      await this.api.solicitation.closeCash({ ...this.registerCash, terminal: { ...this.auth.terminal } })
    } catch (error) {
      this.loading.dismiss()
      this.ngOnInit()
      return this.api.handlerError(error)
    }

    await this.storage.delete('blockRegister')

    this.navCtrl.setRoot(OpenCashRegisterPage)

  }

  async doLogOut() {
    this.navCtrl.setRoot(LoginPage)
    await this.storage.delete('register')
    await this.storage.delete('user')
    this.auth.user = {};
    this.auth.boxIsOpen = false;
    this.loading.dismiss()

  }

  get totalFilled() {
    return this.registerCash.details.reduce((acc, obj) => acc + obj.filled, 0)
  }


}
