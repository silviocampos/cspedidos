import { Component, ViewChild, ElementRef, AfterContentChecked } from '@angular/core';
import { NavController, ModalController, ToastController, ActionSheetController, AlertController, LoadingController, Loading, PopoverController, Platform, MenuController, Events } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { AuthService } from '../../services/auth/auth.service';
import { ApiService } from '../../services/api/api.service';
import { CurrencyPipe } from '@angular/common';
import { StorageService } from '../../services/storage/storage.service';

import moment from 'moment';

import { OpenCashRegisterPage } from '../open-cash-register/open-cash-register';
import { AppUpdate } from '@ionic-native/app-update';
import { MenuPopoverPage } from '../menu-popover/menu-popover';
import Objects from '../../util/objects';
import { PrinterController } from '../../services/printer/printer';
import { range } from 'lodash';
import * as uuid from 'uuid'

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage implements AfterContentChecked {

  @ViewChild('qtd') qtd: ElementRef
  @ViewChild('code') code: ElementRef
  @ViewChild('totalAmountPaymentRef') totalAmountPaymentRef: ElementRef;

  date: any;

  formSale: FormGroup;

  items: any[] = [];

  totalAmountPayment: number = 0;

  payments: any[] = []

  loading: Loading;
  isLoading: boolean = false;

  resend: boolean = false;

  sale: any = {};

  limitItemsView: number = 3;

  users: any[] = [];

  core = (<any>window).cordova;

  paymentsList: any[] = []

  productsToCancel = null;

  UUID_ORDER = null;

  constructor(
    public navCtrl: NavController,
    public modalCtrl: ModalController,
    public toastCtrl: ToastController,
    public alertCtrl: AlertController,
    public auth: AuthService,
    public api: ApiService,
    public storage: StorageService,
    public actionSheet: ActionSheetController,
    public formBuilder: FormBuilder,
    public loadingCtrl: LoadingController,
    public popover: PopoverController,
    public appUpdate: AppUpdate,
    public platform: Platform,
    public menuCtrl: MenuController,
    public events: Events
  ) {
  }

  ngAfterContentChecked() {
    this.date = moment()
    this.auth.orders = this.auth.orders;
  }

  openMenu() {

    this.menuCtrl.enable(true);
    this.menuCtrl.open()
  }

  async ngOnInit() {

    this.events.subscribe("open_orders", () => { this.productsToCancel = null; this.items = []; this.payments = [] })

    this.events.subscribe("change", ({ sale, productsToCancel }) => {
      if (!this.productsToCancel)
        this.doSearchItem(sale[0]);
      this.productsToCancel = productsToCancel;
      return false;
    })

    this.events.subscribe("setPayment", payment => {
      if (this.items.length > 0) {
        if (!this.productsToCancel) {
          payment.amount = this.totalAmount;
          this.setPayment(payment)
        }
      }
    })

    if (this.auth.boxIsOpen && this.auth.user.isAdmin === false && this.auth.search.openProductList) {
      this.doSearchItem()
    }

    this.formSale = this.formBuilder.group({
      qtd: [1, Validators.required],
      code: [null, Validators.required]
    })

    this.sale = await this.storage.get('last')
    this.paymentsList = this.auth.payments
      .filter(payment => !payment.method.includes('TROCA'))
      .filter(payment => !payment.method.includes('CORTESIA'))
      .filter(payment => !payment.method.includes('AVARIA'))

  }

  ngOnDestroy() {
    this.events.unsubscribe("change")
  }

  doOpenMenuPopOver(ev: any) {
    const popover = this.popover.create(MenuPopoverPage)
    popover.present({ ev })
  }


  get valueFormSalve(): { qtd: number, code: number } {
    return this.formSale.value
  }

  get totalItems() {
    if (this.items.length > 0)
      return this.items.reduce((acc, obj) => acc + obj.qtd, 0)

    return 0
  }

  get totalAmount() {

    if (this.items.length > 0)
      return this.items.reduce((acc, obj) => acc + obj.amount, 0)

    return 0
  }

  get totalAmountCredit() {

    const creditPayments = this.payments.filter(({ method }) => method.includes('TROCA'))

    if (creditPayments.length > 0)
      return creditPayments.reduce((acc, obj) => acc + obj.amount, 0)

    return 0
  }



  doAddItem() {


    if (this.valueFormSalve.qtd < 0) {
      this.valueFormSalve.qtd = Math.abs(this.valueFormSalve.qtd)
    }

    const findProductInItems = this.items.find(product => product.code == this.valueFormSalve.code)
    const findProductInList = this.auth.products.filter(product => product.isActive).find(product => product.code == this.valueFormSalve.code);

    if (findProductInItems) {
      findProductInItems.qtd += this.valueFormSalve.qtd;
      findProductInItems.amount = findProductInItems.price * findProductInItems.qtd;
    } else {

      if (!findProductInList)
        return this.notFoundProduct();

      let product = Object.assign({}, findProductInList) as any;
      product.qtd = this.valueFormSalve.qtd;
      product.amount = product.price * product.qtd

      this.items.unshift(product)

    }

    this.totalAmountPayment = this.totalAmount - this.totalAmountCredit

    this.formSale.reset()
    this.focusQtd();
    if (!this.payments.find(({ method }) => method.includes('TROCA')))
      this.payments = []
    this.resend = false;
  }

  doSearchItem(sale?) {

    if (!sale)
      this.productsToCancel = null;

    const modal = this.modalCtrl.create('lista-de-produtos', { sale }, { enableBackdropDismiss: false })
    modal.present()

    modal.onDidDismiss((data: { products: any[] }) => {

      if (!data)
        this.productsToCancel = null;

      if (data) {

        data.products.forEach((product, index) => {
          const isItemExists = this.items.find(pro => pro.code == product.code)

          if (isItemExists) {
            isItemExists.qtd += product.qtd
            isItemExists.amount = isItemExists.price * isItemExists.qtd;
            if (this.payments.length === 0)
              this.payments = []
            this.resend = false;
          } else {
            this.items.unshift(product)
            if (this.payments.length === 0)
              this.payments = []
            this.resend = false;
          }

          if (sale) {
            this.totalAmountPayment = sale.amount;
            const paymentCredit = this.auth.payments.filter(({ method }) => method.includes('TROCA'))[0];
            paymentCredit.fixed = true;
            if (index === 0)
              this.setPayment(paymentCredit)
            return;
          }

        })

        this.totalAmountPayment = (this.totalAmount - this.totalPayments < 0) ? 0 : this.totalAmount - this.totalPayments
      }


    })

    return false;

  }

  doRemoveItem(index) {
    this.items.splice(index, 1)
    this.focusQtd()
    this.totalAmountPayment = this.totalAmount
  }

  doAdditionQtd(index) {



    if (this.items[index].qtd >= 1)
      this.items[index].qtd += 1

    this.multiplicationAmount(index)
    this.totalAmountPayment = this.totalAmount

    this.payments = [...this.payments.filter((pay) => pay.fixed)]
    this.resend = false;




  }

  doSubtractionQtd(index) {
    if (this.items[index].qtd > 1)
      this.items[index].qtd -= 1

    this.multiplicationAmount(index)
    this.totalAmountPayment = this.totalAmount

    const calc = ((this.totalAmount
      - this.totalPayments) < 0) ? 0 : this.totalAmount - this.totalPayments

    if (calc > 0) {
      this.resend = false;
      this.payments = [...this.payments.filter((pay) => pay.fixed)]
    }
    else
      this.resend = true;


  }

  setLoading() {
    this.loading = this.loadingCtrl.create({ content: 'carregando...' })
    this.loading.present()
    this.isLoading = true;
  }

  printerOk = false;

  doOrder() {

    if (this.isLoading === true)
      return;


    const sendOrder = async (operacao = null, note = null) => {


      if (!this.UUID_ORDER)
        this.UUID_ORDER = uuid.v1()


      this.setLoading()

      const terminal = Objects.clone(this.auth.terminal)

      if (terminal.printer.print.app === true) {
        terminal.printer.address = null
      }

      let new_list_items = []

      const unwind = (items: any[]) => new Promise(resolve => {

        const total_items = items.map(({ qtd }) => qtd).reduce((acc, value) => acc + value, 0)

        this.items.forEach(item => {

          if (item.qtd > 1) {
            (<Number[]>range(item.qtd)).forEach(() => {
              const _item_add_list = Object.assign({}, item)
              _item_add_list.qtd = 1;
              _item_add_list.amount = item.price;
              new_list_items = [...new_list_items, _item_add_list]
            })
          } else {
            new_list_items = [...new_list_items, item]
          }

          if (new_list_items.length === total_items)
            resolve()


        })

      })

      await unwind(this.items)

      let order: any = {
        register: this.auth.boxIsOpen,
        customer: 999,
        items: [...new_list_items.map(item => { return { complement: item.name, pccod: item.pccod, product: item.id_product, price: operacao ? 0 : item.price, qtd: item.qtd, amount: operacao ? 0 : item.amount } })],
        payments: [...this.payments],
        summary: {
          amount: operacao ? 0 : this.totalAmount,
          discount: 0,
          increase: 0,
          change: this.totalPayments - this.totalAmount
        },
        user: this.auth.user._id,
        note,
        operacao,
        uuid: this.UUID_ORDER
      }

      try {


        if (this.productsToCancel) {

          let order: any = {
            register: this.auth.boxIsOpen,
            customer: 999,
            items: [...this.productsToCancel.info.items.map(item => {
              const { id_product, name, unit, price, pccod } = this.auth.products.find(product => product.name === item.complement)
              return {
                product: id_product,
                complement: name,
                qtd: item.qtd,
                unit,
                price,
                amount: price * item.qtd,
                pccod,
                _id: item._id,
                owner: item.owner,
                uuid: this.UUID_ORDER
              }

            })],
            // payments: [...this.auth.payments.filter(payment => payment.method.includes("TROCA")).map(({ _id, pccod, docref, acronym, method }) => {
            //   return { payment: _id, pccod, docref, acronym, method, amount: this.productsToCancel.info.items.reduce((acc, obj) => acc + obj.price * obj.qtd, 0), change_money: 0 }
            // })],
            summary: {
              amount: this.productsToCancel.info.items.reduce((acc, obj) => acc + obj.price * obj.qtd, 0),
              discount: 0,
              increase: 0,
              change: 0
            },
            user: this.auth.user._id
          }

          await this.api.solicitation.sale.delete({ order, order_ref: this.productsToCancel._id })

        }

        const objOrder = await this.api.solicitation.order({ order, terminal }) as any;


        if (terminal.printer.print.app && terminal.printer.print.receiptAsItem) {

          const orderPrinter = await this.api.solicitation.sale.printerApp(objOrder.order) as any
          const printerCtrl = new PrinterController(this.auth)

          let cortesia = order.payments.filter(payment => payment.method.includes('CORTE') || payment.method.includes("AVARIA"))

          if (cortesia.length > 0)
            orderPrinter.payments = cortesia.map(p => ({ payment: p.method, amount: 0 }))

          printerCtrl.printerOrder(orderPrinter)

        }

      } catch (error) {

        this.resend = true;
        this.printerOk = true;

        this.loading.dismiss()
        this.isLoading = false;
        return this.api.handlerError(error)
      }


      if (order.summary.change <= 0) {

        this.items = [];
        this.payments = []
        this.totalAmountPayment = this.totalAmount
        this.loading.dismiss()
        this.isLoading = false;
        this.resend = false;
        this.UUID_ORDER = null;
        this.productsToCancel = null;

      } else {

        const currency = new CurrencyPipe('pt-BR')

        const _alert = this.alertCtrl.create({
          title: `${currency.transform(order.summary.change, 'BRL', 'symbol')}`,
          message: `Troco`,
          buttons: ['Ok']
        })

        _alert.onDidDismiss(async () => {
          if (this.auth.search.openProductList)
            this.doSearchItem()
        })

        return _alert.present().then(() => {
          this.items = [];
          this.payments = []
          this.totalAmountPayment = this.totalAmount
          this.loading.dismiss()
          this.isLoading = false;
          this.resend = false;
          this.productsToCancel = null;
          this.UUID_ORDER = null;
        })

      }


      this.printerOk = false;

      if (this.auth.search.openProductList)
        this.doSearchItem()

    }


    const _alert = this.alertCtrl.create({
      title: `Permissão`,
      subTitle: `Items como cortesia, funcionario ou avaria precisam da liberação do seu gerente.`,
      inputs: [

        {
          type: 'textarea',
          id: 'note',
          name: 'note',
          placeholder: 'Justificativa'
        },
        {
          type: 'password',
          id: 'password',
          name: 'password',
          placeholder: 'Senha de gerente.'
        }
      ],
      buttons: [

        {
          text: `Cancelar`,
          role: 'cancel'
        },
        {
          text: 'confirmar',
          handler: ({ password, note }) => {

            this.api.solicitation.isAdmin(password)
              .then((data: any) => {

                if (!data.isAdmin) {
                  this.resend = false;
                  return this.api.handlerError({ error: `Senha admin incorreta! tente novamente.` })
                } else {

                  const avaria = this.payments.filter(payment => payment.method.includes('AVARIA'))

                  if (avaria.length > 0)
                    return sendOrder(44, note)
                  else
                    return sendOrder(502, note)
                }

              })


          }

        }
      ]
    })

    if (this.items.findIndex(item => (item.price <= 0)) >= 0 || this.payments.filter(payment => payment.method.includes('CORTE') || payment.method.includes("AVARIA")).length > 0) {
      _alert.present()
    } else {
      return sendOrder()
    }


  }


  removePayment(i) {

    console.log(this.payments[i], this.payments[i].amount + (this.totalAmount - this.totalPayments < 0) ? 0 : this.totalAmount - this.totalPayments)

    this.totalAmountPayment = (this.totalAmount - this.totalPayments < 0) ? 0 : this.totalAmount - this.totalPayments + this.payments[i].amount;
    this.payments.splice(i, 1)
    this.resend = false;
  }


  async finishOrderFree() {
    const paymentDinheiro = this.auth.payments.find(payment => payment.acronym === 'DIN')

    this.setPayment(paymentDinheiro)
    await this.doOrder()

  }

  async setPayment({ method, _id, acronym, pccod, docref, fixed }) {


    if (this.resend)
      return this.doOrder()

    const isPay = this.payments.findIndex(pay => pay.method === method && fixed);

    if (isPay > -1)
      this.payments[isPay] = { payment: _id, pccod, docref, acronym, method, amount: this.totalAmountPayment + this.payments[isPay].amount, change_money: 0 }
    else
      this.payments.unshift({ payment: _id, pccod, docref, acronym, method, amount: this.totalAmountPayment, change_money: 0, fixed })

    if (fixed) {
      setTimeout(() => this.totalAmountPayment = this.totalAmount - this.totalPayments, 300)
    }


    if (method === 'DINHEIRO') {
      this.payments[0].change_money = (this.totalPayments - this.totalAmount) < 0 ? 0 : this.totalPayments - this.totalAmount
    }

    if (this.totalPayments >= this.totalAmount) {
      this.resend = true;

    } else {
      this.totalAmountPayment = this.totalAmount - this.totalPayments
    }
  }

  get totalPayments() {
    return this.payments.reduce((acc, obj) => acc + obj.amount, 0)
  }

  doOpenSales() {
    const modal = this.modalCtrl.create('resumo-de-vendas')
    modal.present()
  }

  openCashBox() {
    this.navCtrl.push(OpenCashRegisterPage)
  }

  private focusQtd() {
    this.formSale.get('qtd').setValue(1)
    this.formSale.get('code').reset()
    setTimeout(() => {
      this.qtd.nativeElement.focus()
    }, 500)
  }

  private focusCode() {
    setTimeout(() => {
      this.code.nativeElement.focus()
    }, 500)
  }

  private multiplicationAmount(index) {
    this.items[index].amount = (this.items[index].price * this.items[index].qtd)
  }

  private async notFoundProduct() {
    const toast = this.toastCtrl.create({ message: `Produto não encontrado...`, showCloseButton: true, closeButtonText: 'Ok', position: 'middle', duration: 5000 })
    await toast.present()

    toast.onDidDismiss(() => this.focusCode())
  }


}
