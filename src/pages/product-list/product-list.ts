import { Component } from '@angular/core';
import { IonicPage, ViewController, ToastController, NavParams, NavController } from 'ionic-angular';

import { AuthService } from '../../services/auth/auth.service';
import Objects from '../../util/objects';
import { StorageService } from '../../services/storage/storage.service';
import { ApiService } from '../../services/api/api.service';
import { HomePage } from '../home/home';

@IonicPage({
  name: 'lista-de-produtos'
})
@Component({
  selector: 'page-product-list',
  templateUrl: 'product-list.html',
})
export class ProductListPage {

  public products: any[] = []
  public list: boolean = false;

  qtd: any = 1;
  code: any = null

  constructor(
    public view: ViewController,
    public auth: AuthService,
    public storage: StorageService,
    public toastCtrl: ToastController,
    public api: ApiService,
    public navParams: NavParams,
    public navController: NavController
  ) { }

  async ngOnInit() {
    this.products = Objects.clone(this.auth.products);
    this.list = await this.storage.get('list')
    this.products = this.products.filter(product => product.isActive)
  }

  async doCloseModal() {
    await this.view.dismiss()
  }

  doSelectItem() {

    const doFilterOnlyWithQuantity: any[] = this.products.filter(product => product.qtd)

    const doMultiplicationAmountPrice: any[] = doFilterOnlyWithQuantity.map(product => {
      const price = product.price * product.qtd

      return { amount: price, ...product }
    })


    this.view.dismiss(
      { products: doMultiplicationAmountPrice }
    )
  }

  doAdditionQtd(product, qtd = 1) {

    if (!product.qtd)
      product.qtd = qtd
    else
      product.qtd += qtd


  }

  doSubtractionQtd(product) {
    if (product.qtd > 0)
      product.qtd -= 1

  }


  async gridOrList() {
    (this.list) ? this.list = false : this.list = true;

    await this.storage.set('list', this.list)

  }


  get itemsQtdPlus() {
    return this.products.filter(product => product.qtd > 0).length;
  }



  doAddItem() {

    if (!this.qtd || !this.code)
      return;

    if (this.qtd < 0) {
      this.qtd = Math.abs(this.qtd)
    }

    const findProductInItems = this.products.find(product => product.code == this.code)

    this.doAdditionQtd(findProductInItems, this.qtd)


    const toast = this.toastCtrl.create({ message: `${this.qtd} ${findProductInItems.name} adicionado(a) a lista.`, duration: 3000 })

    toast.present()

    this.qtd = 1;
    this.code = null;


  }

  async doUpdateListProducts() {
    await this.auth.setProducts(await this.api.solicitation.productList(this.auth.user.category))
    const toast = this.toastCtrl.create({ message: 'Lista de produtos atualizada..', duration: 3000 })
    toast.present()
    this.products = []
    this.ngOnInit()

  }

}
