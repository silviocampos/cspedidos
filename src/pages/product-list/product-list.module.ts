import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProductListPage } from './product-list';

import { VirtualScrollerModule } from 'ngx-virtual-scroller';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    ProductListPage,
  ],
  imports: [
    VirtualScrollerModule,
    PipesModule,
    IonicPageModule.forChild(ProductListPage),
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class ProductListPageModule { }
