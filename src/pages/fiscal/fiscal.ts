import { Component } from '@angular/core';
import { FormBuilder, FormGroup, } from '@angular/forms';
import { IonicPage, NavController, NavParams, LoadingController, Loading, AlertController } from 'ionic-angular';
import { ApiService } from '../../services/api/api.service';


@IonicPage()
@Component({
  selector: 'page-fiscal',
  templateUrl: 'fiscal.html',
})
export class FiscalPage {

  loading: Loading;
  formFiscal: FormGroup;

  constructor(public navCtrl: NavController, public navParams: NavParams, public formBuilder: FormBuilder, public api: ApiService, public loadingCtrl: LoadingController, public alertCtrl: AlertController) {
  }

  ngOnInit() {
    this.formFiscal = this.formBuilder.group({
      cpf: [null],
      nome: [null],
      email: [null],
    })
  }

  calc_digits_positions(digits, positions = 10, sum = 0) {
    digits = digits.toString()
    for (let i = 0; i < digits.length; i++) {
      sum = sum + (digits[i] * positions)
      positions--
      if (positions < 2) {
        positions = 9
      }
    }
    sum = sum % 11
    if (sum < 2) {
      sum = 0
    } else {
      sum = 11 - sum
    }
    let cpf = digits + sum
    return cpf
  }
  valid_cpf = (value) => {
    value = value.toString()
    value = value.replace(/[^0-9]/g, '')
    let digits = value.substr(0, 9)
    let new_cpf = this.calc_digits_positions(digits)
    new_cpf = this.calc_digits_positions(new_cpf, 11)
    if (new_cpf === value) {
      return true
    } else {
      return false
    }
  }
  async doLogin() {

    this.setLoading()

    const { sale } = this.navParams.data;
    const value = this.formFiscal.value;

    sale.info.cliente = {
      cpf: value.cpf,
      nome: value.nome,
      email: value.email
    }

    if (typeof value.cpf === "number") {
      if (!this.valid_cpf(value.cpf)) {
        await this.loading.dismiss()
        return this.api.handlerError({ error: 'CPF Invalido!' })
      }
    } else if (value.cpf !== null) {
      await this.loading.dismiss()
      return this.api.handlerError({ error: 'CPF Invalido!' })
    }

    const _alert = this.alertCtrl.create({
      title: `Sucesso!`,
      message: `SAT foi emitido com sucesso!`,
      buttons: [
        {
          text: 'Ok',
        }
      ]
    })

    await this.api.solicitation.sat({ ...sale, mvcod: sale._id })
      .then(() => {
        _alert.present();
        this.navCtrl.pop()
      })
      .catch(e => this.api.handlerError({ error: e }))

    await this.loading.dismiss()


  }

  setLoading() {
    this.loading = this.loadingCtrl.create({ content: 'carregando...' })
    this.loading.present()
  }

}
