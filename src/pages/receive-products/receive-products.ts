import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators, AbstractControl, ValidatorFn, ValidationErrors, FormArray } from '@angular/forms';
import { ApiService } from '../../services/api/api.service';


export function requiredIf(requiredIf: { value: boolean }): ValidatorFn {

  return (control: AbstractControl): ValidationErrors | null => {

    let value = control.value;

    alert(value)

    if ((value == null || value == undefined || value == "") && requiredIf.value) {
      return {
        requiredIf: { condition: requiredIf.value }
      };
    }
    return null;
  }
}

@Component({
  selector: 'page-receive-products',
  templateUrl: 'receive-products.html',
})
export class ReceiveProductsPage {

  view = null;

  form_order: FormGroup;

  orders: any[] = [];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    public api: ApiService
  ) {




  }

  async ngOnInit() {

    this.form_order = this.formBuilder.group({
      items: this.formBuilder.array([])
    })

   const orders = await this.api.solicitation.orderEntry() as any

    this.orders = orders || []

  }


  addItem(item_product) {
    const item: FormGroup = this.formBuilder.group({
      id: [item_product._id],
      name: [item_product.complement],
      qtd: [item_product.qtd],
      total: [item_product.amount],
      selected: [null, [Validators.requiredTrue]]
    })

    return item;
  }

  async openOrder(order) {

    const items_array: FormArray = this.form_order.get('items') as FormArray

    const reset = (formArray) => new Promise(resolve => {

      while (formArray.length !== 0) {
        formArray.removeAt(0)
      }

      if (formArray.length === 0)
        resolve()

    })


    await reset(items_array)

    if (this.view === order._id || typeof order != 'object') {
      this.view = null
      return;
    }
    else
      this.view = order._id;

    const { items } = await this.api.solicitation.orderDetail(order._id) as any

    items.forEach(_item => {
      items_array.push(this.addItem(_item))
    })





  }


  async closeOrder() {

    try {
      await this.api.solicitation.closeOrder(this.view)
    } catch (error) {
      return alert('Erro ao processar pedido!')
    }

    const indexOrder = this.orders.findIndex(order => order._id === this.view)

    this.orders.splice(indexOrder, 1)

    this.view = null


  }


}
