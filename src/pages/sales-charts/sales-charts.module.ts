import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SalesChartsPage } from './sales-charts';

@NgModule({
  declarations: [
    SalesChartsPage,
  ],
  imports: [
    IonicPageModule.forChild(SalesChartsPage),
  ],
})
export class SalesChartsPageModule {}
