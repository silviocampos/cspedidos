import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController, Loading, LoadingController, AlertController, ToastController, NavParams } from 'ionic-angular';

import { ApiService } from '../../services/api/api.service';
import { AuthService } from '../../services/auth/auth.service';

@Component({
  selector: 'page-sangria',
  templateUrl: 'sangria.html',
})
export class SangriaPage {

  @ViewChild('amountInput') amountInput: ElementRef;

  amount: any;

  actionSangria: 'up' | 'down';
  loading: Loading;

  constructor(
    public navCtrl: NavController,
    public api: ApiService,
    public auth: AuthService,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public toast: ToastController,
    public navParams: NavParams
  ) {
    this.focus()
    this.actionSangria = this.navParams.get('action');
  }

  focus() {
    setTimeout(() => this.amountInput.nativeElement.focus(), 500)
  }

  toggleAction(action: 'up' | 'down') {

    return;
    // if (action === this.actionSangria) {
    //   this.actionSangria = null
    //   return;
    // }

    // this.actionSangria = action;

    // this.focus()


  }

  async doSangria() {

    const handler: any = async ({ password }) => {

      this.setLoading()

      const { isAdmin } = await this.api.solicitation.isAdmin(password) as any
      if (!isAdmin) {
        this.loading.dismiss()
        return this.api.handlerError({ error: `Senha admin incorreta! tente novamente.` })
      }

      if (isAdmin === true) {

        try {
          if (this.actionSangria == "up")
            await this.api.solicitation.pickup(this.amount)
          else
            await this.api.solicitation.entry(this.amount)

          this.navCtrl.pop()

        } catch (error) {
          this.loading.dismiss()
          return this.api.handlerError(error)
        }

        this.loading.dismiss()


        const toast = this.toast.create({ message: `Sangria realizada com sucesso!`, duration: 3000 })
        toast.present()



      }

    }

    const _alert = this.alertCtrl.create({
      title: `Permissão`,
      message: `Digite a senha do administrador/gerente`,
      inputs: [
        {
          type: 'password',
          id: 'password',
          name: 'password'
        }
      ],
      buttons: [

        {
          text: `Cancelar`,
          role: 'cancel'
        },
        {
          text: 'confirmar',
          handler
        }
      ]
    })

    _alert.present()

  }

  setLoading() {
    this.loading = this.loadingCtrl.create({ content: 'carregando...' })
    this.loading.present()
  }

}
