import { Component, ViewChild, ElementRef, AfterContentChecked } from '@angular/core';
import { NavController, ModalController, ToastController, ActionSheetController, AlertController, LoadingController, Loading, PopoverController } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../../services/auth/auth.service';
import { ApiService } from '../../services/api/api.service';
import { StorageService } from '../../services/storage/storage.service';
import * as uuid from 'uuid'

import moment from 'moment';
import { SettingsPage } from '../settings/settings';

@Component({
  selector: 'close-volantes',
  templateUrl: 'close-volantes.html'
})
export class CloseVolantesPage implements AfterContentChecked {

  @ViewChild('qtd') qtd: ElementRef
  @ViewChild('code') code: ElementRef

  date: any;

  formSale: FormGroup;

  items: any[] = [];

  totalAmountPayment: number = 0;

  ticket: number = 0;

  payments: any[] = []

  loading: Loading;

  resend: boolean = false;

  sale: any = {};

  limitItemsView: number = 3;

  users: any[] = [];

  vendor: any = null;

  paymentsList: any[]

  UUID_ORDER = null;

  constructor(
    public navCtrl: NavController,
    public modalCtrl: ModalController,
    public toastCtrl: ToastController,
    public alertCtrl: AlertController,
    public auth: AuthService,
    public api: ApiService,
    public storage: StorageService,
    public actionSheet: ActionSheetController,
    public formBuilder: FormBuilder,
    public loadingCtrl: LoadingController,
    public popover: PopoverController
  ) {
  }

  ngAfterContentChecked() {
    this.date = moment()
  }

  async ngOnInit() {

    this.formSale = this.formBuilder.group({
      qtd: [1, Validators.required],
      code: [null, Validators.required]
    })

    this.users = await this.api.solicitation.mananger.person() as any
    this.paymentsList = this.auth.payments
      .filter(payment => !payment.method.includes('TROCA'))
      .filter(payment => !payment.method.includes('CORTESIA'))
      .filter(payment => !payment.method.includes('AVARIA'))

  }

  doOpenMenuPopOver(ev: any) {
    const popover = this.popover.create('menu')
    popover.present({
      ev
    })
  }


  get valueFormSalve(): { qtd: number, code: number } {
    return this.formSale.value
  }

  get totalItems() {
    if (this.items.length > 0)
      return this.items.reduce((acc, obj) => acc + obj.qtd, 0)

    return 0
  }

  get totalAmount() {
    if (this.items.length > 0)
      return this.items.reduce((acc, obj) => acc + obj.amount, 0)

    return 0
  }



  doAddItem() {

    if (this.valueFormSalve.code == 1015) {
      this.formSale.reset()
      return this.navCtrl.push(SettingsPage)
    }

    if (this.valueFormSalve.qtd < 0) {
      this.valueFormSalve.qtd = Math.abs(this.valueFormSalve.qtd)
    }

    const findProductInItems = this.items.find(product => product.code == this.valueFormSalve.code)
    const findProductInList = this.auth.products.find(product => product.code == this.valueFormSalve.code);

    if (findProductInItems) {
      findProductInItems.qtd += this.valueFormSalve.qtd;
      findProductInItems.amount = findProductInItems.price * findProductInItems.qtd;
    } else {

      if (!findProductInList)
        return this.notFoundProduct();

      let product = Object.assign({}, findProductInList) as any;
      product.qtd = this.valueFormSalve.qtd;
      product.amount = product.price * product.qtd

      this.items.unshift(product)

    }

    this.totalAmountPayment = this.totalAmount

    this.formSale.reset()
    this.focusQtd()
  }

  doSearchItem() {

    const modal = this.modalCtrl.create('lista-de-produtos', {}, { enableBackdropDismiss: false })
    modal.present()

    modal.onDidDismiss((data: { products: any[] }) => {

      if (data) {

        data.products.forEach((product) => {
          const isItemExists = this.items.find(pro => pro.code == product.code)

          if (isItemExists) {
            isItemExists.qtd += product.qtd
            isItemExists.amount = isItemExists.price * isItemExists.qtd
          } else {
            this.items.unshift(product)
          }

        })

        this.totalAmountPayment = this.totalAmount
      }


    })

    return false;

  }

  doRemoveItem(index) {
    this.items.splice(index, 1)
    this.focusQtd()
    this.totalAmountPayment = this.totalAmount
  }

  doAdditionQtd(index) {
    if (this.items[index].qtd >= 1)
      this.items[index].qtd += 1

    this.multiplicationAmount(index)
    this.totalAmountPayment = this.totalAmount
  }

  doSubtractionQtd(index) {
    if (this.items[index].qtd > 1)
      this.items[index].qtd -= 1

    this.multiplicationAmount(index)
    this.totalAmountPayment = this.totalAmount

  }

  setLoading() {
    this.loading = this.loadingCtrl.create({ content: 'carregando...' })
    this.loading.present()
  }

  doOrder() {

    const sendOrder = async () => {

      if (!this.UUID_ORDER)
        this.UUID_ORDER = uuid.v1()

      let order: any = {
        register: 0,
        customer: 999,
        items: [...this.items.map(item => { return { complement: item.name, pccod: item.pccod || null, product: item.id_product, price: item.price, qtd: item.qtd, amount: item.amount } })],
        payments: [...this.payments],
        summary: {
          amount: this.totalAmount,
          discount: 0,
          increase: 0,
          change: 0
        },
        user: this.auth.user._id,
        vendor: this.vendor._id,
        vendor_name: this.vendor.name,
        ticket: this.ticket,
        uuid: this.UUID_ORDER
      }

      this.setLoading()

      try {

        await this.api.solicitation.orderVendor(
          {
            order,
            credit: (this.totalPayments - this.totalAmount) < 0 ? 0 : this.totalPayments - this.totalAmount,
            debit: (this.totalPayments - this.totalAmount) < 0 ? Math.abs(this.totalPayments - this.totalAmount) : 0,
            terminal: {
              ...this.auth.terminal
            }
          }
        )

      } catch (error) {
        this.loading.dismiss()
        return this.api.handlerError(error)
      }

      this.items = [];
      this.payments = []
      this.totalAmountPayment = this.totalAmount
      this.loading.dismiss()
      this.resend = false;
      this.vendor = null;
      this.ticket = 0;
      this.UUID_ORDER = null;

    }


    const _alert = this.alertCtrl.create({
      title: 'Confimar operação',
      message: `Deseja finalizar fechamento do volante?`,
      buttons: [
        {
          text: 'Sim',
          handler: () => sendOrder() as any
        },
        {
          text: 'Ainda não',
          role: 'cancel'
        }
      ]
    })


    return _alert.present()

  }


  removePayment(i) {

    this.totalAmountPayment = this.payments[i].amount;
    this.payments.splice(i, 1)
    this.resend = false;
  }


  async setPayment({ method, _id, acronym, pccod, docref }) {

    if (this.resend)
      return this.doOrder()

    const isPay = this.payments.findIndex(pay => pay.method === method);

    if (isPay > -1)
      this.payments[isPay] = { payment: _id, pccod, docref, acronym, method, amount: this.totalAmountPayment + this.payments[isPay].amount, change_money: 0 }
    else
      this.payments.unshift({ payment: _id, pccod, docref, acronym, method, amount: this.totalAmountPayment, change_money: 0 })

    if (method === 'DINHEIRO') {
      this.payments[0].change_money = 0
    }

    if (this.totalPayments >= this.totalAmount) {
      this.resend = true;
    } else {
      this.totalAmountPayment = this.totalAmount - this.totalPayments
    }
  }

  get totalPayments() {
    return this.payments.reduce((acc, obj) => acc + obj.amount, 0)
  }

  doOpenSales() {
    const modal = this.modalCtrl.create('resumo-de-vendas')
    modal.present()
  }

  private focusQtd() {
    this.formSale.get('qtd').setValue(1)
    this.formSale.get('code').reset()
    setTimeout(() => {
      this.qtd.nativeElement.focus()
    }, 500)
  }

  private focusCode() {
    setTimeout(() => {
      this.code.nativeElement.focus()
    }, 500)
  }

  private multiplicationAmount(index) {
    this.items[index].amount = (this.items[index].price * this.items[index].qtd)
  }

  private async notFoundProduct() {
    const toast = this.toastCtrl.create({ message: `Produto não encontrado...`, showCloseButton: true, closeButtonText: 'Ok', position: 'middle', duration: 5000 })
    await toast.present()

    toast.onDidDismiss(() => this.focusCode())
  }


}
