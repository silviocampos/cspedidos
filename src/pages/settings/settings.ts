import { Component } from '@angular/core';
import { NavController, ToastController, AlertController, Platform } from 'ionic-angular';

import { ApiService } from '../../services/api/api.service';
import { StorageService } from '../../services/storage/storage.service';
import { AuthService } from '../../services/auth/auth.service';
import { HomePage } from '../home/home';
import { AppVersion } from '@ionic-native/app-version';

import { PrinterController } from '../../services/printer/printer';



@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage {


  loading: boolean | number = false;

  baseURL: string = null;
  port: number = 3000

  slide1: boolean = true
  slide2: boolean = false;
  slide3: boolean = false;

  core = (<any>window).cordova;

  terminal = this.auth.terminal
  param: any = {}
  categories: any = [];

  advanced: boolean = false;

  printerMessage: string = "Testar impressora"

  versionApp: any = 0;

  printerOk: boolean = false;

  operacoes: any = []

  constructor(
    public navCtrl: NavController,
    public api: ApiService,
    public storage: StorageService,
    public auth: AuthService,
    public toast: ToastController,
    public alertCtrl: AlertController,
    public appVersion: AppVersion,
    public platform: Platform
  ) { }

  async ngOnInit() {

    const isTerminal = await this.storage.get('terminal')

    if (isTerminal)
      this.terminal = isTerminal


    this.platform.ready()
      .then(async () => {
        if (this.core)
          this.versionApp = await this.appVersion.getVersionNumber()
        else
          this.versionApp = "2.5.0 no-android"

      })

    this.operacoes = await this.api.solicitation.mananger.operators()
  }

  async autoConfig() {

    const defineSettings = async ({ text }) => {


      if (text) {

        this.printerOk = false;

        const settings = JSON.parse(text)


        if (this.auth.user.account_bank) {
          this.terminal.printer = {
            address: settings.printer.address,
            print: { ...this.terminal.printer.print, app: true }
          }
          this.printerOk = false;
        } else {

          this.terminal = {

            branch: settings.branch,
            branch_ref: settings.branch_ref,
            printer: {
              address: settings.printer.address,
              print: { ...settings.printer.print, app: true }
            },
            register: this.auth.user.account_bank || settings.register,
            _id: settings._id,
            baseURL: settings.baseURL,
            port: settings.port,
            update: settings.baseURL,
            operacao_barraca: this.auth.terminal.operacao_barraca || null,
            fechamento_cego: true
          }
        }

      }
    }

    const error = error => this.api.handlerError({ error: `Scanning failed: ${error}` });

    if (this.core)
      this.core.plugins
        .barcodeScanner
        .scan(defineSettings, error);

  }

  async saveSettings() {


    const toast = this.toast.create({ message: `Configurações salvas com sucesso`, duration: 3000 })

    this.loading = true;

    await this.storage.set('baseURL', `http://${this.terminal.baseURL}:${this.terminal.port || 3000}`);


    try {
      await this.api.solicitation.ping()

      if (this.printerOk === false && this.terminal.printer.address) {
        await this.testerPrinter() as any
      }

      await this.api.solicitation.categories()
    } catch (error) {
      this.loading = false;
      return this.api.handlerError(error)
    }

    if (this.terminal.printer.print.app == undefined || this.terminal.printer.print.app == null)
      this.terminal.printer.print.app = true;

    await this.storage.set('terminal', { ...this.terminal })
    await this.storage.set('settings', true)
    await this.storage.set('search', this.auth.search)

    this.auth.terminal = { ...this.terminal } as any

    this.auth.terminal.update = `http://${this.terminal.baseURL}:${this.terminal.port || 3000}`

    this.loading = false;

    await toast.present()

    if (this.navCtrl.canGoBack())
      await this.navCtrl.pop()
    else
      await this.navCtrl.setRoot(HomePage)



  }

  settingsAdvanced() {

    const _alert = this.alertCtrl.create({
      title: `Permissão`,
      message: `Digite a senha do administrador  do sistema`,
      inputs: [
        {
          type: 'password',
          id: 'password',
          name: 'password'
        }
      ],
      buttons: [

        {
          text: `Cancelar`,
          role: 'cancel'
        },
        {
          text: 'confirmar',
          handler: ({ password }) => {
            if (password === 'cn1VISAO-*')
              this.advanced = true;
          }
        }
      ]
    })

    _alert.present()
  }





  async testerPrinter() {


    this.printerMessage = "Testando..."


    const printNew = this.terminal.printer.address;
    const printCurrent = this.auth.terminal.printer.address

    if (printNew != printCurrent)
      this.auth.terminal.printer.address = printNew

    try {


      if (this.terminal.printer.print.app) {
        const printer = new PrinterController(this.auth)
        await printer.printerTester()
      } else {
        const print = await this.api.solicitation.printerTest() as any
        this.printerOk = print.printer
      }
    } catch (error) {
      console.log(JSON.stringify(error))
      this.api.handlerError(error)
    }

    this.printerMessage = "Testar impressora"

  }


}
