import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { NavController, NavParams, Loading, LoadingController } from 'ionic-angular';

import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import moment from 'moment';
import { ApiService } from '../../services/api/api.service';
import { AuthService } from '../../services/auth/auth.service';
import { StorageService } from '../../services/storage/storage.service';
import { HomePage } from '../home/home';
import Objects from '../../util/objects';
import { LoginPage } from '../login/login';

@Component({
  selector: 'page-open-cash-register',
  templateUrl: 'open-cash-register.html',
})
export class OpenCashRegisterPage implements OnInit {

  @ViewChild('valueInitCash') valueInitCash: ElementRef;

  formOpenCash: FormGroup;

  date = moment()

  loading: Loading;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public api: ApiService,
    public auth: AuthService,
    public storage: StorageService,
    public formBuilder: FormBuilder,
    public loadingCtrl: LoadingController
  ) { }

  ngOnInit() {

    this.formOpenCash = this.formBuilder.group({
      amount: [null, Validators.required],
    })

    setTimeout(() => this.valueInitCash.nativeElement.focus(), 500)

  }

  async doOpenCash() {

    this.setLoading()

    try {

      let terminalClone = Objects.clone(this.auth.terminal) //clone do terminal
      terminalClone.register = this.auth.user.account_bank //força update do register para account_bank

      const { register } = await this.api.solicitation.registerCash({ terminal: { ...terminalClone }, amount: this.formOpenCash.value.amount }) as any
      await this.auth.setRegister(register)
      this.loading.dismiss()

      this.navCtrl.setRoot(HomePage)

    } catch (error) {
      this.loading.dismiss()

      return this.api.handlerError(error)
    }



  }

  async doLogOut() {
    await this.navCtrl.setRoot(LoginPage)
    await this.storage.delete('user')
    await this.storage.delete('register')
    this.auth.user = {}
    this.auth.boxIsOpen = false;
  }

  setLoading() {
    this.loading = this.loadingCtrl.create({ content: 'carregando...' })
    this.loading.present()
  }

}
