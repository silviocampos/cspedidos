import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Loading, AlertController, Events, ModalController } from 'ionic-angular';

import { ApiService } from '../../services/api/api.service';

@IonicPage({
  name: 'resumo-de-vendas'
})
@Component({
  selector: 'page-sales-summary',
  templateUrl: 'sales-summary.html',
})
export class SalesSummaryPage {
  venda = 0

  salesSummary: { out: any[], in: any[] } = { out: [], in: [] }
  loading: Loading;

  ordersType: 'out' | 'int' = 'out'

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public api: ApiService,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public events: Events,
    public modalCtrl: ModalController
  ) { }

  async ngOnInit() {

    this.setLoading()

    try {
      const sales: any[] = await this.api.solicitation.orderList() as any || [];

      const salesSummary = sales.map(sale => { return { ...sale, expanded: false } })
      this.salesSummary.in = salesSummary.filter(order => order.type === 'in' && order.sequence !== "null")
      this.salesSummary.out = salesSummary.filter(order => order.type === 'out' && order.sequence !== "null")

    } catch (error) {
      this.loading.dismiss()
      this.api.handlerError(error)
    }

    this.loading.dismiss()


    this.events.subscribe("updateOrders", ({ update }) => {
      if (update) {
        this.ngOnInit()
      }
    })



  }

  tabChange({ value }) {
    this.ordersType = value
  }

  doOpenCardSale(sale) {
    this.salesSummary[this.ordersType].map(async saleItem => {

      if (sale == saleItem) {
        saleItem.expanded = !saleItem.expanded;
      } else {
        saleItem.expanded = false;
      }

      if (saleItem.info)
        delete saleItem.info

      if (saleItem.expanded)
        saleItem.info = await this.api.solicitation.orderDetail(saleItem._id)

      return saleItem;

    });
  }

  setLoading() {
    this.loading = this.loadingCtrl.create({ content: 'carregando...' })
    this.loading.present()
  }


  closeToSummary() {
    this.navCtrl.pop()
  }






}
