import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SalesSummaryPage } from './sales-summary';
import { ComponentsModule } from '../../components/components.module';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    SalesSummaryPage,
  ],
  imports: [
    ComponentsModule,
    PipesModule,
    IonicPageModule.forChild(SalesSummaryPage),
  ],
})
export class SalesSummaryPageModule { }
