import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, LoadingController, Loading, AlertController, MenuController } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ApiService } from '../../services/api/api.service';
import { AuthService } from '../../services/auth/auth.service';
import { StorageService } from '../../services/storage/storage.service';
import { HomePage } from '../home/home';
import { SettingsPage } from '../settings/settings';
import { OpenCashRegisterPage } from '../open-cash-register/open-cash-register';
import { CloseCashRegisterPage } from '../close-cash-register/close-cash-register';
import { PrinterController } from '../../services/printer/printer';


@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage implements OnInit {

  formLogin: FormGroup;

  loading: Loading

  categories: any = []

  online: boolean = false

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public api: ApiService,
    public auth: AuthService,
    public storage: StorageService,
    public formBuilder: FormBuilder,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public menuCtrl: MenuController
  ) { }

  async  ngOnInit() {

    this.menuCtrl.enable(false);

    this.formLogin = this.formBuilder.group({
      login: [null, Validators.required],
      password: [null, [Validators.required, Validators.minLength(3)]],
      category: [null, [Validators.required]]
    })
  }

  async ionViewDidEnter() {

    try {
      await this.api.solicitation.ping()
      this.categories = await this.api.solicitation.categories()
      this.storage.set('categories', this.categories)
    } catch (error) {
      this.api.handlerError(error)
    }

  }


  async doLogin() {

    this.setLoading()

    const terminal = await this.storage.get('terminal');
    terminal.register = terminal._id;

    this.api.solicitation.login({ ...this.formLogin.value, terminal })
      .then(async (data) => {


        const { token, user, register, account_bank, success, message } = data as any;


        if (success === false)
          await this.storage.set('blockRegister', true);


        const continueLogin: any = async ({ openCloseRegister }) => {


          user.category = this.formLogin.value.category;

          if (user.isActive === false) throw alert('Usuario está desativado...')


          await this.auth.setUser({ token, ...user })

          await this.auth.setProducts(await this.api.solicitation.productList(this.auth.user.category))

          await this.auth.setPayments(await this.api.solicitation.paymentsList())

          await this.auth.setTerminal(account_bank)

          const { isAdmin } = await this.api.solicitation.isAdmin(this.formLogin.value.password) as any

          user.isAdmin = ((user.rule === 'admin' || user.rule === 'manager') === isAdmin) ? true : false

          await this.auth.setUser({ token, ...user })



          if (!register && !this.auth.user.isAdmin)
            this.navCtrl.setRoot(OpenCashRegisterPage)
          else {

            await this.auth.setRegister(register)

            if (register != 0) {
              const { close } = await this.api.solicitation.register() as any;
              await this.loading.dismiss()
              if (close)
                return await this.navCtrl.setRoot(OpenCashRegisterPage);
            }

            if (openCloseRegister)
              await this.navCtrl.push(CloseCashRegisterPage)
            else
              await this.navCtrl.setRoot(HomePage)
          }

          try {

            if (!this.auth.terminal.printer.print.app && this.auth.terminal.printer.address)
              await this.api.solicitation.printerTest()

            if (this.auth.terminal.printer.print.app) {
              const printer = new PrinterController(this.auth)
              await printer.printerTester()
            }


          } catch (error) {
            this.loading.dismiss()

            this.api.handlerError(error)
          }

          this.loading.dismiss()

        }


        const confirmDuplicationSales = this.alertCtrl.create({
          title: 'Aviso!!!',
          message: 'Atenção, esse procedimento vai acumular suas vendas ao movimento do outro usuário, tem certeza que quer fazer isso?'.toLocaleUpperCase(),
          buttons: [{
            text: 'Sim',
            handler: () => continueLogin({ openCloseRegister: false })
          }, {
            text: 'Fechar o caixa dele',
            handler: () => continueLogin({ openCloseRegister: true })
          }]
        })

        const existsAccountBank = this.alertCtrl.create({
          title: 'Aviso!!!',
          message: (<String>message).toLocaleUpperCase(),
          buttons: [{
            text: 'Fechar o caixa dele',
            handler: () => continueLogin({ openCloseRegister: true })
          }, {
            text: 'Continuar movimentacao',
            handler: () => confirmDuplicationSales.present()
          }]
        })

        if (success === false) {
          await existsAccountBank.present()
          this.loading.dismiss()
        }
        else
          continueLogin({ openCloseRegister: false })



      })
      .catch(error => {
        this.loading.dismiss();
        this.api.handlerError(error)
      })


  }

  setCategory(_id) {
    this.formLogin.get('category').setValue(_id)
  }

  setLoading() {
    this.loading = this.loadingCtrl.create({ content: 'carregando...' })
    this.loading.present()
  }

  openSettings() {
    this.navCtrl.push(SettingsPage, { initialize: false })
  }

}
