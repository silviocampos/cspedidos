import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, ModalController, AlertController, ToastController } from 'ionic-angular';
import { AuthService } from '../../services/auth/auth.service';
import { CloseVolantesPage } from '../close-volantes/close-volantes';
import { LoginPage } from '../login/login';
import { SettingsPage } from '../settings/settings';
import { OpenCashRegisterPage } from '../open-cash-register/open-cash-register';
import { CloseCashRegisterPage } from '../close-cash-register/close-cash-register';
import { RegistersClosedPage } from '../registers-closed/registers-closed';
import { SangriaPage } from '../sangria/sangria';
import { SalesSummaryPage } from '../sales-summary/sales-summary';
import { ReceiveProductsPage } from '../receive-products/receive-products';
import { CloseBarracasPage } from '../close-barracas/close-barracas';
import { ApiService } from '../../services/api/api.service';


@Component({
  selector: 'page-menu-popover',
  templateUrl: 'menu-popover.html',
})
export class MenuPopoverPage {

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public auth: AuthService,
    public api: ApiService,
    public view: ViewController,
    public modalCtrl: ModalController,
    public alert: AlertController,
    public toast: ToastController,
  ) { }


  openSettings() {
    this.navCtrl.push(SettingsPage);
    this.view.dismiss()

  }

  doOpenSales() {
    const modal = this.modalCtrl.create(SalesSummaryPage)
    modal.present()
    this.view.dismiss()

  }

  doCloseCash() {
    this.navCtrl.push(CloseCashRegisterPage)
    this.view.dismiss()

  }

  async doOpenCloseVolantes() {
    await this.navCtrl.push(CloseVolantesPage)
    await this.view.dismiss()
  }

  async doOpenCloseBarracas() {
    await this.navCtrl.push(CloseBarracasPage)
    await this.view.dismiss()
  }

  async doLogOut() {

    await this.view.dismiss()

    const goLogin = async () => {
      this.navCtrl.setRoot(LoginPage)
      this.auth.logout()
    }

    const confirmExit = this.alert.create({
      title: `Confirmação`,
      message: `Seu caixa não foi fechado, deseja fechar caixa agora?`,
      buttons: [
        {
          text: `Não fechar caixa`,
          handler: () => {
            setTimeout(() => goLogin(), 100)
          }
        }, {
          text: `Fechar caixa`,
          handler: () => {
            this.navCtrl.push(CloseCashRegisterPage)
          }
        }
      ]
    })


    if (this.auth.boxIsOpen)
      confirmExit.present()
    else
      await goLogin()

  }

  async doSangria(action: 'up' | 'down') {
    await this.navCtrl.push(SangriaPage, { action })
    await this.view.dismiss()
  }

  doOpenCash() {
    this.navCtrl.push(OpenCashRegisterPage)
    this.view.dismiss()
  }

  doOpenRegistersClosed() {
    this.navCtrl.push(RegistersClosedPage)
    this.view.dismiss()
  }

  doOpenStock() {
    this.navCtrl.push(ReceiveProductsPage)
    this.view.dismiss()
  }

  async doUpdateListProducts() {
    await this.auth.setProducts(await this.api.solicitation.productList(this.auth.user.category))
    this.view.dismiss();
    const toast = this.toast.create({ message: 'Lista de produtos atualizada..', duration: 3000 })
    toast.present()
  }

}
