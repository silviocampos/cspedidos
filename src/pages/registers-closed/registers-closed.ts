import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, Loading, LoadingController } from 'ionic-angular';
import { AuthService } from '../../services/auth/auth.service';
import { ApiService } from '../../services/api/api.service';

import * as moment from 'moment';


@Component({
  selector: 'page-registers-closed',
  templateUrl: 'registers-closed.html',
})
export class RegistersClosedPage {

  public registers: any[] = []

  loading: Loading

  date = moment().format('YYYY-MM-DD');

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public auth: AuthService,
    public api: ApiService,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController
  ) { }

  async ngOnInit() {

    this.setLoading()

    const dates = { startDate: moment(this.date).format('YYYY-MM-DD'), endDate: moment(this.date).format('YYYY-MM-DD') }

    try {
      const registers = await this.api.solicitation.mananger.registers(dates) as any[]
      this.registers = registers.filter(register => register.closeAt)
    } catch (error) {
      this.loading.dismiss()
      this.api.handlerError(error)
    }

    this.loading.dismiss()



  }


  setLoading() {
    this.loading = this.loadingCtrl.create({ content: 'carregando...' })
    this.loading.present()
  }

  rePrint(register) {

    const print = async (type) => {

      this.setLoading()

      let request;

      if (type === 'closed')
        request = this.api.solicitation.mananger.printClose(register.register);
      else if (type === 'orders')
        request = this.api.solicitation.mananger.printProduct(register.register)

      try {
        await request
      } catch (error) {
        this.loading.dismiss()
        return this.api.handlerError(error)
      }

      this.loading.dismiss()


    }

    const confirmRePrint = this.alertCtrl.create({
      title: `Caixa - ${register.operator}`,
      message: `Imprimir fechamento ou vendas?`,
      buttons: [
        {
          text: `Imprimir fechamento`,
          handler: () => {
            print('closed')
          }
        }, {
          text: `Imprimir vendas`,
          handler: () => {
            print('orders')
          }
        }, {
          text: `Cancelar`,
          role: 'cancel'
        }
      ]
    })

    confirmRePrint.present()
  }

  registersPrintsProducts: any[] = [];

  rePrintProducts(register) {

    const isRegisterSelected = this.registersPrintsProducts.indexOf(register.register);

    if (isRegisterSelected === -1) {
      this.registersPrintsProducts.unshift(register.register)
      register.selected = true;
    } else {
      this.registersPrintsProducts.splice(isRegisterSelected, 1)
      delete register.selected
    }

    return false;
  }

  async printAllProducts() {

    this.setLoading()

    this.loading.setContent('Processando...')

    try {
      await this.api.solicitation.mananger.printProduct(this.registersPrintsProducts)
    } catch (error) {
      this.loading.dismiss()
      return this.api.handlerError(error)
    }

    this.loading.dismiss()

    this.registersPrintsProducts = [];

    this.registers.forEach((register, index) => {
      delete this.registers[index].selected
    })


  }

}
