import { Component, ViewChild } from '@angular/core';
import { Platform, Nav, Keyboard, MenuController, ModalController, AlertController, ToastController, Events } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Autostart } from '@ionic-native/autostart';
import { AuthService } from '../services/auth/auth.service';
import { StorageService } from '../services/storage/storage.service';
import { ApiService } from '../services/api/api.service';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { SettingsPage } from '../pages/settings/settings';
import { AppUpdate } from '@ionic-native/app-update';
import { SalesSummaryPage } from '../pages/sales-summary/sales-summary';
import { CloseCashRegisterPage } from '../pages/close-cash-register/close-cash-register';
import { CloseVolantesPage } from '../pages/close-volantes/close-volantes';
import { CloseBarracasPage } from '../pages/close-barracas/close-barracas';
import { SangriaPage } from '../pages/sangria/sangria';
import { OpenCashRegisterPage } from '../pages/open-cash-register/open-cash-register';
import { RegistersClosedPage } from '../pages/registers-closed/registers-closed';
import { ReceiveProductsPage } from '../pages/receive-products/receive-products';
import { AppVersion } from '@ionic-native/app-version';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {

  @ViewChild(Nav) navCtrl: Nav;

  core = (<any>window).cordova;

  versionApp: any;
  paymentsList: any[] = [];

  constructor(
    public platform: Platform,
    public statusBar: StatusBar,
    private autostart: Autostart,
    public splashScreen: SplashScreen,
    public auth: AuthService,
    public storage: StorageService,
    public api: ApiService,
    public keyboard: Keyboard,
    public appUpdate: AppUpdate,
    public menuCtrl: MenuController,
    public modalCtrl: ModalController,
    public alertCtrl: AlertController,
    public toastCtrl: ToastController,
    public appVersion: AppVersion,
    public events: Events
  ) {
    this.menuCtrl.enable(false);
    this.initialize()
  }

  disabledBackButton() {

    const disabled = () => {

      if (this.keyboard.isOpen())
        this.keyboard.close()

      return false;

    }

    this.platform.registerBackButtonAction(disabled)
  }

  enableAutoStart() {
    return this.autostart.enable()
  }

  async getSettings() {
    return await this.storage.get('settings')
  }

  async getRegister() {
    return await this.storage.get('register')
  }

  async getTerminal() {
    return await this.storage.get('terminal')
  }

  async getSearchOptions() {
    return await this.storage.get('search')
  }

  async getAllUser() {

    const getStorage = (db) => this.storage.get(db)

    let [
      blockRegister,
      products,
      payments,
      user] = await Promise.all([getStorage('blockRegister'), getStorage('products'), getStorage('payments'), getStorage('user')])

    if (blockRegister)
      return false

    if (products)
      this.auth.products = [...products]

    if (payments) {
      this.paymentsList = payments.filter(payment => payment.method.includes('CORTESIA'))
      this.auth.payments = [...payments]
    }

    if (user)
      this.auth.user = { ...user }

    if (products && payments && user)
      return true

    return null

  }

  async initialize() {



    const ready = await this.platform.ready()

    if (ready) {


      if (this.core)
        this.versionApp = await this.appVersion.getVersionNumber()
      else
        this.versionApp = "2.5.0 no-android"


      this.disabledBackButton()
      this.enableAutoStart()

      const [settings, register, terminal, allUser, search] = await Promise.all([await this.getSettings(),
      await this.getRegister(),
      await this.getTerminal(),
      await this.getAllUser(),
      await this.getSearchOptions()])

      this.splashScreen.hide()
      this.statusBar.styleDefault()

      if (!settings || !terminal) {
        await this.navCtrl.setRoot(LoginPage, { init: true })
        return this.navCtrl.push(SettingsPage)
      }

      this.auth.terminal = { ...terminal }
      this.auth.terminal.update = terminal.baseURL
      this.auth.boxIsOpen = register;


      if (search) {
        this.auth.search = search

        if (typeof search.openProductList != 'boolean')
          search.openProductList = true

      }
      else
        this.auth.search = { description: false, orderByDescription: false, openProductList: true }

      this.checkUpdate()

      if (!allUser)
        return this.navCtrl.setRoot(LoginPage)

      return this.navCtrl.setRoot(LoginPage)


    }

  }


  checkUpdate(click = false) {

    let onSuccess = (success) => {
      if (success.code === 202) {
        if (click)
          this.toastCtrl.create({ message: 'Sua versão é a mais atual.', duration: 2000, position: 'bottom' }).present()
      }

    }

    let onFail = (err) => console.log(JSON.stringify(err))

    let updateURL = this.auth.terminal.update;

    this.appUpdate.checkAppUpdate(`http://${updateURL}:${3000}/update`)
      .then(onSuccess)
      .catch(onFail)

  }


  openSettings() {
    this.navCtrl.push(SettingsPage);

  }

  doOpenSales() {

    this.events.publish("open_orders");

    const modal = this.modalCtrl.create(SalesSummaryPage)
    modal.present()

  }

  doCloseCash() {
    this.navCtrl.push(CloseCashRegisterPage)

  }

  async doOpenCloseVolantes() {
    await this.navCtrl.push(CloseVolantesPage)
  }

  async doOpenCloseBarracas() {
    await this.navCtrl.push(CloseBarracasPage)
  }

  async doLogOut() {


    const goLogin = async () => {
      this.navCtrl.setRoot(LoginPage)
      this.auth.logout()
    }

    const confirmExit = this.alertCtrl.create({
      title: `Confirmação`,
      message: `Seu caixa não foi fechado, deseja fechar caixa agora?`,
      buttons: [
        {
          text: `Não fechar caixa`,
          handler: () => {
            setTimeout(() => goLogin(), 100)
          }
        }, {
          text: `Fechar caixa`,
          handler: () => {
            this.navCtrl.push(CloseCashRegisterPage)
          }
        }
      ]
    })


    if (this.auth.boxIsOpen)
      confirmExit.present()
    else
      await goLogin()

  }

  async doSangria(action: 'up' | 'down') {
    await this.navCtrl.push(SangriaPage, { action })

  }

  doOpenCash() {
    this.navCtrl.push(OpenCashRegisterPage)

  }

  doOpenRegistersClosed() {
    this.navCtrl.push(RegistersClosedPage)
  }

  doOpenRegistersCars() {
    this.navCtrl.push('CloseCarsPage')
  }

  doOpenStock() {
    this.navCtrl.push(ReceiveProductsPage)
  }

  async doUpdateListProducts() {
    await this.auth.setProducts(await this.api.solicitation.productList(this.auth.user.category))
    const toast = this.toastCtrl.create({ message: 'Lista de produtos atualizada..', duration: 3000 })
    toast.present()
  }



  setPaymentCortesia() {

    const cortesia = this.auth.payments.filter(payment => payment.method.includes('CORTESIA'))

    if(cortesia.length === 0){
      alert('Cortesia não ativada!');
      return;
    }

    this.events.publish("setPayment", cortesia[0])
  }

  setPaymentAvaria(){

    const avaria = this.auth.payments.filter(payment => payment.method.includes('AVARIA'))

    if(avaria.length === 0){
      alert('Avaria não ativada!');
      return;
    }

    this.events.publish("setPayment", avaria[0])
  }

}

