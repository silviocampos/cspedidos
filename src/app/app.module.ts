import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule, LOCALE_ID } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { registerLocaleData } from '@angular/common';
import ptBr from '@angular/common/locales/pt';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { CURRENCY_MASK_CONFIG } from 'ng2-currency-mask/src/currency-mask.config';

import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { QRScanner } from '@ionic-native/qr-scanner';
import { AppVersion } from '@ionic-native/app-version';
import { AppUpdate } from '@ionic-native/app-update';
import { MyApp } from './app.component';

import { CustomCurrencyMaskConfig } from '../util/envioriment';

import { ApiService } from '../services/api/api.service';
import { AuthService } from '../services/auth/auth.service';
import { StorageService } from '../services/storage/storage.service';
import { IonicStorageModule } from '@ionic/storage';

import { Autostart } from '@ionic-native/autostart';
import { HomePage } from '../pages/home/home';
import { PipesModule } from '../pipes/pipes.module';
import { CurrencyMaskModule } from 'ng2-currency-mask';
import { CloseVolantesPage } from '../pages/close-volantes/close-volantes';
import { DirectivesModule } from '../directives/directives.module';
import { NgxMaskModule } from 'ngx-mask';
import { LoginPage } from '../pages/login/login';
import { SettingsPage } from '../pages/settings/settings';
import { OpenCashRegisterPage } from '../pages/open-cash-register/open-cash-register';
import { CloseCashRegisterPage } from '../pages/close-cash-register/close-cash-register';
import { RegistersClosedPage } from '../pages/registers-closed/registers-closed';
import { MenuPopoverPage } from '../pages/menu-popover/menu-popover';
import { SangriaPage } from '../pages/sangria/sangria';

import { SalesSummaryPageModule } from '../pages/sales-summary/sales-summary.module';
import { ReceiveProductsPage } from '../pages/receive-products/receive-products';
import { CloseBarracasPage } from '../pages/close-barracas/close-barracas';
import { FiscalPageModule } from '../pages/fiscal/fiscal.module';

registerLocaleData(ptBr);

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    CloseVolantesPage,
    CloseBarracasPage,
    LoginPage,
    SettingsPage,
    OpenCashRegisterPage,
    CloseCashRegisterPage,
    RegistersClosedPage,
    MenuPopoverPage,
    SangriaPage,
    ReceiveProductsPage
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    PipesModule,
    CurrencyMaskModule,
    DirectivesModule,
    NgxMaskModule,
    SalesSummaryPageModule,
    FiscalPageModule,
    IonicStorageModule.forRoot({
      driverOrder: ['localstorage'],
      name: '___patati'
    }),
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    CloseVolantesPage,
    CloseBarracasPage,
    LoginPage,
    SettingsPage,
    OpenCashRegisterPage,
    CloseCashRegisterPage,
    RegistersClosedPage,
    MenuPopoverPage,
    SangriaPage,
    ReceiveProductsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    ScreenOrientation,
    Autostart,
    QRScanner,
    AppVersion,
    AppUpdate,
    { provide: ApiService, useClass: ApiService },
    { provide: AuthService, useClass: AuthService },
    { provide: StorageService, useClass: StorageService },
    { provide: LOCALE_ID, useValue: 'pt-BR' },
    { provide: CURRENCY_MASK_CONFIG, useValue: CustomCurrencyMaskConfig },
    { provide: ErrorHandler, useClass: IonicErrorHandler }
  ]
})
export class AppModule { }
